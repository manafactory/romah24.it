<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 23/01/17
 * Time: 11.11
 * Template Name: Registrazione
 */


get_header(); ?>


		<?php
		while ( have_posts() ) : the_post();
        ?>
      <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

          <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">

              <?php get_template_part('template-parts/content', 'sharing');?>

            <div class="post-content red_link">
                <?php if (is_user_logged_in()):?>

                    <h1><?php echo get_the_title();?></h1>
                    <?php $current_user=wp_get_current_user();?>

                    <h2>Ciao <?php echo $current_user->data->display_name;?>, sei già loggato.</h2>

                <?php else:?>
                    <h1><?php echo get_the_title();?></h1>
                    <h2>Accedi o registrati al sito e scopri tutti i vantaggi.</h2>
                    <hr class="hr_login">
                    <?php get_template_part('template-parts/content', 'social-login');?>
                <?php endif;?>
            </div>




          </div><!-- /col-md-8 -->

            <?php get_sidebar();?>


        </div><!-- /row -->

      </main>

		<?php endwhile; // End of the loop.
		?>
    </div><!-- /container -->
<?php
get_footer();
