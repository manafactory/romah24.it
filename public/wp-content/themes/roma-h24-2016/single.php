<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Roma_H24_2016
 */

get_header(); ?>


		<?php
		while ( have_posts() ) : the_post();
        ?>
      <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

          <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">

              <?php get_template_part('template-parts/content', 'sharing');?>

            <div class="post-content red_link">

                <?php
                $posttype=str_replace('-','_',get_post_type());
                $categoria_post=wp_get_post_terms(get_the_ID(),'categoria_'.$posttype);
                $quartiere=wp_get_post_terms(get_the_ID(),'quartiere');
                ?>
                <h3 class="news_dettaglio_data_e_ora">
                    <?php if ($quartiere):
                        echo '<a href="'.get_term_link($quartiere[0]->term_id).'" title="'.$quartiere[0]->name.'">';
                        echo $quartiere[0]->name;
                        echo '</a>';
                    endif;?>

                    <?php if ($categoria_post):
                        echo ' | <a href="'.get_term_link($categoria_post[0]->term_id).'" title="'.$categoria_post[0]->name.'">';
                        echo $categoria_post[0]->name;
                        echo '</a>';
                    endif;?>
                </h3>
                <h1 class="news-title">
                    <?php the_title();?>
                </h1>
                <?php if (is_singular('annunci')):?>

                    <?php
                    $email=get_field('email_annuncio');
                    $telefono=get_field('telefono_annuncio');
                    $prezzo=get_field('prezzo');
                    ?>
                    <?php if ($email || $prezzo || $telefono):?>
                    <ul class="descr_annuncio">
                        <?php if ($email):
                            if (wp_is_mobile()):
                                echo '<li><a href="mailto:'.$email.'">'.$email.'</a></li>';
                            else:
                                echo '<li>Tel: '.$email.'</li>';
                            endif;
                        endif;?>


                        <?php if ($telefono):
                            if (wp_is_mobile()):
                                echo '<li><a href="tel:+'.$telefono.'">'.$telefono.'</a></li>';
                            else:
                                echo '<li>Tel: '.$telefono.'</li>';
                            endif;
                        endif;?>

                        <?php if ($prezzo): echo '<li>Prezzo: '.$prezzo.'</li>'; endif;?>
                    </ul>
                    <?php endif;?>
                <?php endif;?>
                <?php if (has_post_thumbnail()):?>
                    <figure>
                        <?php $thumb_description=get_thumb_descritpion(get_the_ID());?>
                        <?php $default_attr = array('class'	=> "img-responsive",'alt'	=> $thumb_description);?>
                        <?php the_post_thumbnail('', $default_attr);?>
                        <figcaption><?php echo $thumb_description;?></figcaption>
                    </figure>
                <?php endif;?>
                <?php the_content();?>
                <div class="callbacks_container" style="margin-top: 50px;">

                    <ul class="rslides" id="slider4">
                        <?php $gallery=get_field('gallery_annuncio'); $counter=0;
                        if ($gallery):
                            foreach($gallery as $img): ?>
                                <li>
                                    <img src="<?php echo $img['sizes']['img_gallery_multimedia_single'];?>" alt="foto<?php echo $counter+1;?><?php if ($img['alt']!=''):?>-<?php echo $img['alt'];?><?php endif;?>">
                                    <p class="caption">
                                        foto <?php echo $counter+1;?>
                                        <?php if ($img['alt']!=''):?> - <?php echo $img['alt'];?><?php endif;?></p>
                                </li>

                                <?php
                                $images[]=$img['url'];
                                $counter++;
                            endforeach;
                        endif;
                        ?>
                    </ul>
                </div>


            </div>




          </div><!-- /col-md-8 -->

            <?php get_sidebar();?>


        </div><!-- /row -->

      </main>

		<?php endwhile; // End of the loop.
		?>
    </div><!-- /container -->
<?php
get_footer();
