<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 17/01/17
 * Time: 12.45
 */
?>




<div class="box box-article-rounded-thumb">

    <article>

        <div class="article-wrapper">
            <div class="thumb">
                <?php if (get_post_type(get_the_ID())=='multimedia' && is_video(get_the_ID())):
                    echo '<span class="icon icon-play" aria-hidden="true"></span>';
                endif;?>
                <?php if (has_post_thumbnail()):?>
                    <?php the_post_thumbnail();?>
                <?php else:?>
                    <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/placeholder-350x210.jpg">
                <?php endif;?>
            </div>
            <div class="article-content">
                <?php
                $posttype=str_replace('-','_',get_post_type());
                $categoria_post=wp_get_post_terms(get_the_ID(),'categoria_'.$posttype);
                ?>

                <h2>
                    <a href="<?php echo get_the_permalink();?>">
                        <?php if ($categoria_post && !is_wp_error($categoria_post)):
                            echo $categoria_post[0]->name;
                            echo ' - ';
                        endif;?>

                        <?php echo get_the_title();?>
                    </a>
                </h2>
                <?php if (get_post_type(get_the_ID())=='eventi'):?>

                    <?php $data_evento=get_field('data', get_the_ID());

                    if ($data_evento):
                        $data_array=explode(" ",$data_evento);

                        $data_evento_to_show="";

                        if ($data_array):
                            $data_to_show=$data_array[0];
                            $clock_to_show=$data_array[1];
                            $ore_min_sec=explode(":",$clock_to_show);
                            $clock_to_show= $ore_min_sec[0].':'.$ore_min_sec[1];

                            $data_evento_to_show=show_date($data_to_show).' - '.$clock_to_show;
                        else:

                            $data_evento_to_show=$data_evento;

                        endif;

                        ?>

                        <p><?php echo $data_evento_to_show;?></p>
                    <?php endif;?>
                    <?php $luogo_evento=get_field('luogo', get_the_ID());

                    if ($luogo_evento):
                        ?>

                        <p><?php echo $luogo_evento;?></p>
                    <?php endif;?>
                <?php else:?>
                    <p><?php echo get_the_excerpt();?></p>
                <?php endif;?>
            </div><!-- /article-content -->

        </div><!-- /article-wrapper -->

    </article>

</div><!-- /box-article-rounded-thumb -->

