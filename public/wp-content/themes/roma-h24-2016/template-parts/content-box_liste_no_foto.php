<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 17/01/17
 * Time: 12.45
 */
?>





<?php
global $data_to_check;
$data_news=get_post_time('d:m:Y', false);
$data_news_to_show=get_post_time('d/m/Y', false);
if ($data_news!=$data_to_check):
    $data_to_check=$data_news;

    ?>
    <div class="title-date">
        <p><?php echo show_date($data_news_to_show);?></p>
    </div>
<?php endif;?>

<div class="box box-article">

    <article>

        <div class="article-wrapper">
            <?php $gmt_timestamp = get_post_time('G:i', false);?>
            <div class="time"><?php echo $gmt_timestamp;?></div>

            <div class="article-content">

                <h2>
                    <a href="<?php echo get_the_permalink();?>">
                        <?php
                        $categoria_news=wp_get_post_terms(get_the_ID(),'categoria');
                        ?>
                        <?php if ($categoria_news && !is_wp_error($categoria_post)):
                            echo $categoria_news[0]->name;
                            echo ' - ';
                        endif;?>
                        <?php echo get_the_title();?>
                    </a>
                </h2>
                <?php $sommario=get_field('sommario');?>
                <?php if ($sommario):?>
                    <p><?php echo $sommario;?></p>
                <?php endif;?>
            </div><!-- /article-content -->

        </div><!-- /article-wrapper -->

    </article>

</div><!-- /box-article -->
