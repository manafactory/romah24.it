<div class="col-md-12">
    <?php get_template_part('template-parts/content', 'sharing');?>

    <div class="callbacks_container">

        <ul class="rslides" id="slider4">
            <?php $gallery=get_field('gallery'); $counter=0; $tot_foto=count($gallery);
            foreach($gallery as $img):?>
                <li>
                    <img src="<?php echo $img['sizes']['img_gallery_multimedia_single'];?>" alt="foto<?php echo $counter+1;?>">
                    <p class="caption"> <?php echo ($counter+1).'/'.$tot_foto;?> - <?php echo $img['description'];?></p>
                </li>

                <?php
                $images[]=$img['url'];
                $counter++;
            endforeach;
            ?>
        </ul>
    </div>
    <div class="post-content div_white red_link">
        <?php the_content();?>
    </div>


</div><!-- /col-md-12 -->
