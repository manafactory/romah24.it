<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 06/02/17
 * Time: 17.10
 */

$call_toaction_box_title=get_field('call_to_action_titolo','option');
$call_toaction_box_testo=get_field('call_to_action_testo','option');
$call_toaction_box_link=get_field('call_to_action_link','option');
?>
<?php if (($call_toaction_box_testo || $call_toaction_box_testo) && $call_toaction_box_link):?>
    <div class="panel panel-xs panel-red entra_nella_community">
        <a href="<?php echo $call_toaction_box_link?>" title="<?php if ($call_toaction_box_title): echo $call_toaction_box_title; endif;?>">
            <h3><?php if ($call_toaction_box_title): echo $call_toaction_box_title; endif;?></h3>

            <p><?php if ($call_toaction_box_testo): echo $call_toaction_box_testo; endif;?></p>

            <a class="btn btn-white btn-xs right" href="<?php echo $call_toaction_box_link?>">Clicca qui</a>
        </a>
    </div><!-- /panel -->
<?php endif;?>
