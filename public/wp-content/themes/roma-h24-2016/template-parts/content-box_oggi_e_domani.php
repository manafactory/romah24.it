<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 17/01/17
 * Time: 12.45
 */
?>



<?php $eventi_oggi_domani = eventi_oggi_e_domani(); $counter_oggi=count($eventi_oggi_domani);?>
<?php $eventi_futuri = eventi_futuri(); $counter_futuri=count($eventi_futuri);?>

<?php if ($eventi_oggi_domani || $eventi_futuri):?>
    <div class="owl-carousel carousel-theme carousel_news_group">
        <?php if ($eventi_oggi_domani):?>
            <div class="item">

                <div class="section-title section-title-grey">


                    <h3>Oggi e domani al <?php echo get_bloginfo('name'); ?></h3>

                </div><!-- /section-title -->

                <div class="row">

                    <?php $counter_foreach=0; foreach ($eventi_oggi_domani as $evento): $counter_foreach++;?>

                        <?php if ($counter_foreach==1):?>
                            <div class="col-md-6">

                                <div class="box box-article-simple box-article-simple-large">

                                    <div class="box-article-simple-thumb">
                                        <?php
                                        $thumb_id = get_post_thumbnail_id($evento->ID);

                                        if ($thumb_id):

                                            $thumb_url = wp_get_attachment_image_src($thumb_id, 'news_evidenza', true);

                                            $url_thumb=$thumb_url[0];

                                        else:

                                            $url_thumb=get_bloginfo('template_url').'/assets/placeholders/placeholder-article-simple.jpg';
                                        endif;

                                        ?>


                                        <a href="<?php echo get_permalink($evento->ID);?>"><img src="<?php echo $url_thumb;?>"></a>

                                    </div><!-- /box-article-simple-thumb -->

                                    <div class="box-article-simple-content">

                                        <h2><a href="<?php echo get_permalink($evento->ID);?>"><?php echo $evento->post_title;?></a></h2>
                                        <small class="date"><?php echo show_date(get_field('data', $evento->ID));?></small>

                                        <p><?php echo get_the_excerpt($evento->ID);?></p>

                                    </div><!-- /box-article-simple-content -->

                                </div><!-- /box-article-simple -->

                            </div><!-- /col-md-6 -->
                        <?php else:?>
                            <?php if ($counter_foreach==2):?>
                                <div class="col-md-6">
                            <?php endif;?>
                            <div class="box box-article-simple">

                                <div class="box-article-simple-thumb">

                                    <?php
                                    $thumb_id = get_post_thumbnail_id($evento->ID);

                                    if ($thumb_id):

                                        $thumb_url = wp_get_attachment_image_src($thumb_id, 'news_evidenza', true);

                                        $url_thumb=$thumb_url[0];

                                    else:

                                        $url_thumb=get_bloginfo('template_url').'/assets/placeholders/placeholder-article-simple.jpg';
                                    endif;

                                    ?>


                                    <a href="<?php echo get_permalink($evento->ID);?>"><img src="<?php echo $url_thumb;?>"></a>

                                </div><!-- /box-article-simple-thumb -->

                                <div class="box-article-simple-content">

                                    <h2><a href="<?php echo get_permalink($evento->ID);?>"><?php echo $evento->post_title;?></a></h2>

                                    <small class="date"><?php echo show_date(get_field('data', $evento->ID));?></small>
                                </div><!-- /box-article-simple-content -->

                            </div><!-- /box-article-simple -->

                            <?php if ($counter_foreach==$counter_oggi):?>
                                </div><!-- /col-md-6 -->
                            <?php endif;?>
                        <?php endif;?>
                    <?php endforeach?>
                </div><!-- /row -->

            </div><!-- /item -->
        <?php endif;?>
        <?php if ($eventi_futuri):?>
            <div class="item">

                <div class="section-title section-title-grey">


                    <h3>Prossimi giorni <?php echo get_bloginfo('name'); ?></h3>

                </div><!-- /section-title -->

                <div class="row">

                    <?php $counter_foreach=0; foreach ($eventi_futuri as $evento): $counter_foreach++;?>

                        <?php if ($counter_foreach==1):?>
                            <div class="col-md-6">

                                <div class="box box-article-simple box-article-simple-large">

                                    <div class="box-article-simple-thumb">
                                        <?php
                                        $thumb_id = get_post_thumbnail_id($evento->ID);

                                        if ($thumb_id):

                                            $thumb_url = wp_get_attachment_image_src($thumb_id, 'news_evidenza', true);

                                            $url_thumb=$thumb_url[0];

                                        else:

                                            $url_thumb=get_bloginfo('template_url').'/assets/placeholders/placeholder-article-simple.jpg';
                                        endif;

                                        ?>


                                        <a href="<?php echo get_permalink($evento->ID);?>"><img src="<?php echo $url_thumb;?>"></a>

                                    </div><!-- /box-article-simple-thumb -->

                                    <div class="box-article-simple-content">

                                        <h2><a href="<?php echo get_permalink($evento->ID);?>"><?php echo $evento->post_title;?></a></h2>

                                        <small class="date"><?php echo show_date(get_field('data', $evento->ID));?></small>

                                        <p><?php echo get_the_excerpt($evento->ID);?></p>

                                    </div><!-- /box-article-simple-content -->

                                </div><!-- /box-article-simple -->

                            </div><!-- /col-md-6 -->
                        <?php else:?>
                            <?php if ($counter_foreach==2):?>
                                <div class="col-md-6">
                            <?php endif;?>
                            <div class="box box-article-simple">

                                <div class="box-article-simple-thumb">

                                    <?php
                                    $thumb_id = get_post_thumbnail_id($evento->ID);

                                    if ($thumb_id):

                                        $thumb_url = wp_get_attachment_image_src($thumb_id, 'news_evidenza', true);

                                        $url_thumb=$thumb_url[0];

                                    else:

                                        $url_thumb=get_bloginfo('template_url').'/assets/placeholders/placeholder-article-simple.jpg';
                                    endif;

                                    ?>


                                    <a href="<?php echo get_permalink($evento->ID);?>"><img src="<?php echo $url_thumb;?>"></a>

                                </div><!-- /box-article-simple-thumb -->

                                <div class="box-article-simple-content">

                                    <h2><a href="<?php echo get_permalink($evento->ID);?>"><?php echo $evento->post_title;?></a></h2>

                                    <small class="date"><?php echo show_date(get_field('data', $evento->ID));?></small>

                                </div><!-- /box-article-simple-content -->

                            </div><!-- /box-article-simple -->

                            <?php if ($counter_foreach==$counter_futuri):?>
                                </div><!-- /col-md-6 -->
                            <?php endif;?>
                        <?php endif;?>
                    <?php endforeach?>
                </div><!-- /row -->

            </div><!-- /item -->
        <?php endif;?>

    </div><!-- /carousel_news_group -->

<?php endif;?>
