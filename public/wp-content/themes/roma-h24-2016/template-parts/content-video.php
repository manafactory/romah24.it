<div class="col-md-8">

    <?php get_template_part('template-parts/content', 'sharing');?>

    <div class="post-content red_link">
        <?php $url_video=get_field('url_video');?>
        <?php
        $categoria_news=wp_get_post_terms(get_the_ID(),'categoria_multimedia');
        $quartiere=wp_get_post_terms(get_the_ID(),'quartiere');
        ?>
        <h3 class="news_dettaglio_data_e_ora">
            <?php if ($quartiere):
                echo '<a href="'.get_term_link($quartiere[0]->term_id).'" title="'.$quartiere[0]->name.'">';
                echo $quartiere[0]->name;
                echo '</a>';
            endif;?>

            <?php if ($categoria_news):
                echo ' | <a href="'.get_term_link($categoria_news[0]->term_id).'" title="'.$categoria_news[0]->name.'">';
                echo $categoria_news[0]->name;
                echo '</a>';
            endif;?>
        </h3>
        <h1 class="news-title">
            <?php the_title();?>
        </h1>

        <?php the_content();?>
        <?php
        if (is_video(get_the_ID())):?>


            <?php echo ( wp_oembed_get($url_video)); ?>

        <?php endif;?>
    </div>


    <?php get_template_part('template-parts/content', 'sharing');?>


</div><!-- /col-md-8 -->

<?php get_sidebar('multimedia');?>


