<div class="social_container">
    <div class="col-md-6">
        <div class="wp-social-login-widget">

            <div class="wp-social-login-connect-with">Accedi o registrati con i tuoi social.</div>

            <div class="social_widget_container">

                <div class="wp-social-login-provider-list facebook">

                    <a rel="nofollow" href="http://romah24.dev/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=http%3A%2F%2Fromah24.dev%2Fpagina-registrazione%2F" title="Connect with Facebook" class="wp-social-login-provider wp-social-login-provider-facebook" data-provider="Facebook">
                        <img alt="Facebook" title="Connect with Facebook" src="<?php echo bloginfo('template_url');?>/assets/icons/loghi/mini-facebook.png">
                        Facebook
                    </a>
                </div>
                <div class="wp-social-login-provider-list twitter">

                    <a rel="nofollow" href="http://romah24.dev/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Twitter&amp;redirect_to=http%3A%2F%2Fromah24.dev%2Fpagina-registrazione%2F" title="Connect with Twitter" class="wp-social-login-provider wp-social-login-provider-twitter" data-provider="Twitter">
                        <img alt="Twitter" title="Connect with Twitter" src="<?php echo bloginfo('template_url');?>/assets/icons/loghi/mini-twitter.jpg">
                        Twitter
                    </a>
                </div> <!-- / div.wp-social-login-connect-options -->

                <div class="wp-social-login-widget-clearing"></div>

            </div>

        </div> <!-- / div.wp-social-login-widget -->
    </div>
    <div class="col-md-6 col6testologin">
        <?php if (is_page_template('template-registrati.php')):?>
            <?php the_content();?>
        <?php endif;?>
    </div>
</div>
