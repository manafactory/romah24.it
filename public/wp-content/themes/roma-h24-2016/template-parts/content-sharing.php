<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 17/01/17
 * Time: 12.45
 */
?>



<div class="utilities">
    <div class="utilities-left">
        <a href="#" onclick="window.print();"><span class="icon icon-icon-outline-print-02" aria-hidden="true"></span></a>
        <a href="mailto:?subject=I wanted to share this post with you from <?php bloginfo('name'); ?>&body=<?php the_title(); ?>&#32;&#32;<?php the_permalink(); ?>">
            <span class="icon icon-icon-outline-email-02" aria-hidden="true"></span>
        </a>

    </div><!-- /utilities-left -->


    <?php
    if (has_post_thumbnail(get_the_ID())):
        $fb_thumb=get_the_post_thumbnail_url();
    endif;

    $fb_link='http://www.facebook.com/sharer.php?s=100';
    $fb_link.='&p[title]='.get_the_title();
    $fb_link.='&p[url]='.get_the_permalink();
    $fb_link.='&p[images][0]='.$fb_thumb;

    $tw_link='http://www.twitter.com/share?';
    $tw_link.='text='.get_the_title();
    $tw_link.='&url='.get_the_permalink();

    $gplus_link='http://plus.google.com/share?';
    $gplus_link.='url='.get_the_permalink();
    ?>
    <div class="utilities-right">
            <a href="<?php echo $fb_link;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-facebook.png"></a>

            <a href="<?php echo $tw_link;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-twitter.jpg"></a>

            <a href="<?php echo $gplus_link;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-google+.jpg"></a>
   </div><!-- /utilities-right -->
</div><!-- /utilities -->
