<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Roma_H24_2016
 */

?>


<footer role="contentinfo" id="footer" class="footer">

    <div class="container footer-container">

        <div class="row">

            <div class="col-md-8">

                <?php
                    if (has_nav_menu('menu-footer')):
                ?>

                <div class="footer-links">

                    <?php
                    $args_menu_footer=array(
                        'echo' => false,
                        'container' => false,
                        'items_wrap' => '%3$s',
                        'depth' => 0,
                        'theme_location' => 'menu-footer'
                    );
                    echo strip_tags(wp_nav_menu( $args_menu_footer ), '<a>' );
                    ?>

                </div>
                <?php endif;?>
            </div><!-- /col-md-8 -->

            <div class="col-md-4">

                <a class="logo-typimedia right" href="#"><img src="<?php echo get_bloginfo('template_url');?>/assets/img/typimedia-logo.png"></a>

            </div><!-- /col-md-4 -->

        </div><!-- /row -->

    </div>

</footer><!-- /footer -->

</div><!-- /push_container -->


<?php wp_footer(); ?>

<script src="<?php echo get_bloginfo('template_url');?>/assets/js/scripts.js"></script>

<?php if (is_post_type_archive('news')||is_post_type_archive('eventi')||is_tax('categoria_eventi')):?>
    <script src="<?php bloginfo('template_url');?>/assets/js/clndr/json2.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/clndr/moment-2.8.3.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/clndr/underscore.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/clndr/clndr.js"></script>

    <script type="text/javascript">

        moment.locale('it');

        var events = [
        ];

        jQuery('#mini-clndr').clndr({
            daysOfTheWeek: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
            events: events,
            template: jQuery('#calendar-template').html()
        });

    </script>
<?php endif;?>

<?php
if (current_user_can('administrator')){
    if($_REQUEST["debug"] == "true"){
        global $wpdb;
        echo "<pre>";
        print_r($wpdb->queries);
        echo "</pre>";

    }
}
?>

<?php if (is_singular('multimedia')):?>
    <?php
    if (is_video(get_the_ID())):?>


        <script src="<?php bloginfo('template_url');?>/assets/js/iframe-resize.js"></script>

    <?php endif;?>
    <?php if (is_immagine(get_the_ID())):?>

        <script src="<?php bloginfo('template_url');?>/assets/js/color-box-image.js"></script>
    <?php endif;?>


<?php endif;?>
<?php
if (is_singular('multimedia')||is_singular('annunci')):


    if ((is_immagine(get_the_ID()) && !is_video(get_the_ID())) || is_singular('annunci') && (get_field('gallery_annuncio', get_the_ID()))):?>
        <script src="<?php bloginfo('template_url');?>/assets/js/responsiveslides.min.js"></script>
        <script>
            // You can also use "$(window).load(function() {"
            jQuery(function () {

                // Slideshow 4
                jQuery("#slider4").responsiveSlides({
                    auto: false,
                    pager: false,
                    nav: true,
                    speed: 500,
                    namespace: "callbacks"
                });

            });

            function change_url(){
                var string_to_append= jQuery( "li.callbacks1_on" ).find( "img" ).attr( "alt" );
                margin_img()
                window.history.pushState("object or string", "Title", "#"+string_to_append);
            }

            jQuery(document).ready(function(){
                margin_img()
            });
            function margin_img(){
                var w_container=jQuery( "li.callbacks1_on" ).width();
                var w_img=jQuery( "li.callbacks1_on" ).find( "img" ).width();
                var margin_img=(w_container-w_img)/2;
                jQuery( "li.callbacks1_on" ).find( "img" ).css( "margin-left", margin_img );
            }
        </script>

    <?php endif;?>
<?php endif;?>




</body>

</html>