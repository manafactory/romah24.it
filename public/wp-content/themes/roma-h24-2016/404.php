<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Roma_H24_2016
 */

get_header(); ?>
    <div class="top-title">
        <p class="h1">404</p>
        <div class="breadcrumbs">
            <a href="#">Home</a> > 404</strong>
        </div>
    </div>
    <div class="adv adv-leaderboard">
        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">
    </div><!-- /adv -->
    <main class="main_container" id="main">
        <div class="row row-margin-0 margin-bottom-20">
            <div class="col-md-12">
                <div class="post-content page_404">
                    <h1 class="page404">404</h1>
                    <h3 class="page404">Spiacenti. Pagina non trovata.</h3>
                </div>
            </div><!-- /col-md-8 -->
        </div><!-- /row -->
    </main>
</div>
<?php
get_footer();
