<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Roma_H24_2016
 */

get_header(); ?>


<?php
	while ( have_posts() ) : the_post();
?>
      <div class="top-title">
          <p class="h1"><?php echo get_the_title();?></p>
          <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>
      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

          <div class="row row-margin-0 margin-bottom-20">

              <div class="col-md-8">
                  <?php get_template_part('template-parts/content', 'sharing');?>
                  <div class="row row-margin-0 margin-bottom-20">

                      <div class="col-md-3">
                          <?php if (has_post_thumbnail()):?>
                              <?php the_post_thumbnail('thumbnail');?>
                          <?php endif;?>
                      </div>
                      <div class="col-md-9 content_defunto">
                          <div class="post-content">
                              <p class="h1 nato_morto nome_defunto"><?php echo get_the_title();?></p>
                              <?php $nato=get_field('data_di_nascita');?>
                              <?php $morto=get_field('data_di_morte');?>
                              <?php if ($nato || $morto):?>
                                  <p class="nato_morto">N. <?php echo $nato;?> - M. <?php echo $morto;?></p>
                              <?php endif;?>

                          </div>
                          <div class="post-content">
                              <?php $quartiere=get_quartiere(get_the_id());?>
                              <?php if($quartiere):?>
                                  <?php
                                  $data_news=get_post_time('d:m:Y', false);
                                  $data_news_to_show=get_post_time('d/m/Y', false);
                                  ?>
                                  <p class="h1 nato_morto"><?php echo $quartiere[0]->name;?> - <?php echo show_date($data_news_to_show);?></p>
                              <?php endif;?>
                          </div>
                      </div>
                  </div>
                  <div class="row row-margin-0 margin-bottom-20">
                      <div class="col-md-12">
                          <div class="post-content red_link">
                              <?php the_content();?>
                          </div>
                      </div><!-- /col-md-8 -->
                  </div>
                  <div class="row row-margin-0 margin-bottom-20">
                      <div class="col-md-12">
                          <div class="post-content">
                              <?php comments_template();?>
                          </div>
                      </div><!-- /col-md-8 -->
                  </div><!-- /row -->
              </div><!-- /col-md-8 -->
              <?php get_sidebar('defunti');?>



          </div><!-- /row -->

      </main>

		<?php endwhile; // End of the loop.
		?>
    </div><!-- /container -->
<?php
get_footer();
