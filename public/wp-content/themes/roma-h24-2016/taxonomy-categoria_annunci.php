<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 20/01/17
 * Time: 12.24
 */

get_header(); ?>


<?php if (have_posts()):?>

    <div class="top-title">

        <h1><?php echo (get_queried_object()->name);?></h1>

        <div class="breadcrumbs">
            <a href="<?php echo get_bloginfo('url');?>">Home</a> > <strong><?php echo ucfirst(get_queried_object()->taxonomy);?>: <?php echo get_queried_object()->name;?></strong>
        </div>

    </div>


    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->

      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">

          <?php while(have_posts()): the_post();?>

    <div class="box box-article-rounded-thumb">

        <article>

            <div class="article-wrapper">
                <div class="thumb">
                    <?php if (has_post_thumbnail()):?>
                        <?php the_post_thumbnail();?>
                    <?php else:?>
                        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/placeholder-350x210.jpg">
                    <?php endif;?>
                </div>
                <div class="article-content">
                    <h2><a href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></h2>
                    <p>soooommario</p>

                </div><!-- /article-content -->

            </div><!-- /article-wrapper -->

        </article>

    </div><!-- /box-article-rounded-thumb -->
<?php endwhile;?>


          </div><!-- /col-md-8 -->


        <?php get_sidebar('topics');?>
        </div><!-- /row -->
        <div class="row row-margin-0 margin-bottom-20">
            <div class="col-md-8">
                <div class="pagination_container">
                    <?php my_pagination();?>
                </div>

             </div>
        </div>
      </main>

    </div><!-- /container -->
<?php endif;?>

    </div><!-- /container -->
<?php
get_footer();
