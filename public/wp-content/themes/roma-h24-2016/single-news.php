<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Roma_H24_2016
 */

get_header(); ?>


<?php
	while ( have_posts() ) : the_post();
?>
      <div class="top-title">
        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>
      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">


            <div class="post-content red_link">
                <?php global $data_to_check; $data_to_check='';?>

                <?php
                get_template_part("content");
                ?>
                <nav class="navigation post-navigation" role="navigation">
                    <?php previous_post_link(); ?>
                    <?php next_post_link(); ?>
                </nav><!-- .navigation -->

            </div>




          </div><!-- /col-md-8 -->


            <?php get_sidebar('news');?>

        </div><!-- /row -->

      </main>

		<?php endwhile; // End of the loop.
		?>
    </div><!-- /container -->
<?php
get_footer();
