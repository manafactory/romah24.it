<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Roma_H24_2016
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/assets/css/main-romah24.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/assets/css/style_mod.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/assets/css/style_mod_parent.css">
    <?php if (is_singular('multimedia')):
        if (is_immagine(get_the_ID()) && !is_video(get_the_ID())):?>
            <link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/assets/css/style_dark.css">
        <?php endif;?>
    <?php endif;?>

    <script src="<?php echo get_bloginfo('template_url');?>/assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="<?php echo get_bloginfo('template_url');?>/assets/js/vendor/respond.min.js"></script>

</head>


<body>
<?php remove_action('pre_get_posts', 'filtra_post_per_giorno')?>

<!-- mobile menu -->
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right perfect-scrollbar">

    <div class="menu-mobile-wrapper">

        <div class="top-nav-mobile"></div>

        <div class="menu-mobile"></div>

    </div>

</nav>
<!-- end mobile menu -->
<!-- skin non sarà un background del div-->
<div class="body_wrapper push_container skin-background clearfix">

    <header role="banner">

        <div class="container top-nav">

            <div class="row">

                <div class="col-md-8">

                    <div class="top-nav-wrapper">

                        <div class="top-nav-content">

                            <p>Roma, <?php echo date_i18n('j F Y'); ?></p>

                            <div class="meteo-content">
                                <?php
                                roma_h24_get_meteo();
                                ?>

                            </div><!-- /meteo-content -->

                        </div><!-- /top-nav-content -->

                        <div class="login">
                            <?php if (is_user_logged_in()):?>
                                <?php $current_user=wp_get_current_user();?>

                                <a href="#">Ciao, <?php echo $current_user->data->display_name;?></a>
                                |
                                <a href="http://romah24.dev/wp-login.php?action=logout&_wpnonce=877c0d5184">Logout</a>

                            <?php else:?>
                                <?php $pagina_login=get_field('pagina_di_login', 'option');?>
                                <?php if($pagina_login):?>
                                    <a href="<?php echo $pagina_login;?>">Accedi</a>
                                <?php endif;?>
                            <?php endif;?>
                        </div>

                    </div><!-- /top-nav-wrapper -->

                </div><!-- /col-md-8 -->

                <div class="col-md-4">

                    <div class="top-nav-social">
                        <?php
                        $facebook=get_field('facebook', 'option');
                        $twitter=get_field('twitter', 'option');
                        $google_plus=get_field('google_plus', 'option');
                        $instagram=get_field('instagram', 'option');
                        $pinterest=get_field('pinterest', 'option');
                        ?>
                        <span style="margin-right: 10px;">Seguici su:</span>
                        <?php if ($facebook):?>
                            <a href="<?php echo $facebook;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-facebook.png"></a>
                        <?php endif;?>

                        <?php if ($twitter):?>
                            <a href="<?php echo $twitter;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-twitter.jpg"></a>
                        <?php endif;?>

                        <?php if ($instagram):?>
                            <a href="<?php echo $instagram;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-instagram.png"></a>
                        <?php endif;?>

                        <?php if ($google_plus):?>
                            <a href="<?php echo $google_plus;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-google+.jpg"></a>
                        <?php endif;?>

                        <?php if ($pinterest):?>
                            <a href="<?php echo $pinterest;?>"><img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-pinterest.jpg"></a>
                        <?php endif;?>

                    </div>

                </div><!-- /col-md-4 -->

            </div><!-- /row -->

        </div><!-- /top-nav -->

        <div class="container header-container sticky-wrapper">

            <div class="hamburger hamburger--slider-r toggle-menu menu-right push-body hidden-md">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>

            <div class="row margin-bottom-10">

                <div class="col-md-6">

                    <div class="logo">
                        <?php $logo= get_field('logo_principale', 'option');?>
                        <?php if (is_singular('multimedia')):
                            if (is_immagine(get_the_ID()) && !is_video(get_the_ID() && get_field('logo_secondario', 'option'))):?>
                                <?php $logo= get_field('logo_secondario', 'option');?>
                            <?php endif;?>
                        <?php endif;?>
                        <?php if ($logo):?>
                            <a href="<?php bloginfo('url');?>" title="<?php bloginfo('name');?>">
                                <img class="logo_principale" src="<?php echo $logo['sizes']['logo_principale'];?>" alt="Logo <?php bloginfo('url');?>">
                            </a>
                        <?php else:?>
                            <a href="<?php bloginfo('url');?>">
                                <p class="logo-main">Roma<span>H24</span></p>
                                <h1><?php bloginfo("name"); ?></h1>
                            </a>
                        <?php endif;?>
                    </div>

                </div><!-- /col-md-6 -->

                <div class="col-md-6">

                    <div class="box box-search-form">

                        <form>
                            <input class="form-control" type="text" value="" name="s" placeholder="Cerca..." id="form-search-input" title="Cerca...">
                            <input type="submit" value="">
                        </form>

                    </div><!-- /box-search-form -->

                </div><!-- /col-md-6 -->

            </div><!-- /row -->


            <div class="main-nav">
            <?php
            $args_menu=array(
                'menu_class' => 'menu',
                'echo' => true,
                'container' => 'nav',
                'fallback_cb' => 'wp_page_menu',
                'items_wrap' => '<ul id="%1$s" class="main-menu sf-menu navgoco">%3$s</ul>',
                'depth' => 0,
                'theme_location' => 'menu-principale'
            );
            ?>

            <?php wp_nav_menu($args_menu);?>

            </div><!-- /main-nav -->

        </div><!-- /header-container -->

    </header>







    <div class="container sticky-container">

        <div class="mobile-search">



        </div>