<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Roma_H24_2016
 */

if ( ! is_active_sidebar( 'sidebar-4' ) ) {
	return;
}
?>
<div class="col-md-4">
    <aside class="sidebar">
            <?php dynamic_sidebar( 'sidebar-5' ); ?>
    </aside>
</div>
