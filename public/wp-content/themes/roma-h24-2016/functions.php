<?php
/**
 * Roma H24 2016 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Roma_H24_2016
 */

if ( ! function_exists( 'roma_h24_2016_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function roma_h24_2016_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Roma H24 2016, use a find and replace
         * to change 'roma-h24-2016' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'roma-h24-2016', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        add_theme_support('auto-load-next-post');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        // This theme uses wp_nav_menu() in one location.

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        // Set up the WordPress core custom background feature.
        /*
        add_theme_support( 'custom-background', apply_filters( 'roma_h24_2016_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );
        */
    }
endif;
add_action( 'after_setup_theme', 'roma_h24_2016_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function roma_h24_2016_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'roma_h24_2016_content_width', 640 );
}
add_action( 'after_setup_theme', 'roma_h24_2016_content_width', 0 );




if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function roma_h24_2016_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Home', 'roma-h24-2016' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Inserisci qui i widget.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Generale', 'roma-h24-2016' ),
        'id'            => 'sidebar-2',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella Sidebar Generale.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Lista News', 'roma-h24-2016' ),
        'id'            => 'sidebar-3',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella pagina di lista delle News.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Dettaglio News', 'roma-h24-2016' ),
        'id'            => 'sidebar-8',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella pagina dettaglio News.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Pagine Defunti', 'roma-h24-2016' ),
        'id'            => 'sidebar-4',
        'description'   => esc_html__( 'Sidebar pagine lista e dettaglio Defunti.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Pagine Multimedia', 'roma-h24-2016' ),
        'id'            => 'sidebar-5',
        'description'   => esc_html__( 'Sidebar pagine lista e dettaglio multimedia Video.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Lista Eventi', 'roma-h24-2016' ),
        'id'            => 'sidebar-6',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella pagina di lista degli eventi.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Dettaglio Eventi', 'roma-h24-2016' ),
        'id'            => 'sidebar-9',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella pagina di Dettaglio degli eventi.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Dettaglio Speciali', 'roma-h24-2016' ),
        'id'            => 'sidebar-7',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella pagina dettaglio Speciali.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );


    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Lista Topics', 'roma-h24-2016' ),
        'id'            => 'sidebar-10',
        'description'   => esc_html__( 'Inserisci qui i widget che vuoi visualizzare nella pagina di lista dei post di un Topic.', 'roma-h24-2016' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );



}
add_action( 'widgets_init', 'roma_h24_2016_widgets_init' );




//functions per il recupero degli id delle category multimedia

function get_id_of_multimedia_cat_img(){
    $term_immagine=get_term_by('slug', MM_CAT_IMG_SLUG, 'categoria_multimedia');


    if ($term_immagine):

        return $term_immagine->term_id;

    else:

        return false;

    endif;

}
function get_id_of_multimedia_cat_video(){
    $term_immagine=get_term_by('slug', MM_CAT_VIDEO_SLUG, 'categoria_multimedia');


    if ($term_immagine):

        return $term_immagine->term_id;

    else:

        return false;

    endif;

}








// includo il file che registra tutti i widget
require_once('widget/widget_register.php');


require_once('inc/pre_get_posts.php');




/**
 * Enqueue scripts and styles.
 */
function roma_h24_2016_scripts() {
    wp_enqueue_style( 'roma-h24-2016-style', get_stylesheet_uri() );

    wp_enqueue_script( 'roma-h24-2016-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'roma-h24-2016-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'roma_h24_2016_scripts' );

/**
 * Meteo API
 *
 */
require get_template_directory() . '/inc/meteo.php';

/**
 * Breadcrumb
 */
require get_template_directory() . '/inc/breadcrumb.php';



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



require get_template_directory() . '/inc/register_custom_post_type.php';
require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/inc/define.php';


function pri($da_stampare){

    echo '<pre>';
    print_r($da_stampare);
    echo '</pre>';
}




add_theme_support( 'menus' );

register_nav_menus(
    array(
        'menu-principale' => 'Menu Principale',
        'menu-footer' => 'Menu Footer'
    )
);



add_theme_support( 'post-thumbnails' );

add_image_size( 'news_evidenza', 616, 350, true );
add_image_size( 'coupon_home', 593, 157, true );
add_image_size( 'top_leaderboard', 738, 90, true );
add_image_size( 'logo_news', 104, 35, true );
add_image_size( 'community_book_sidebar', 550, 315, true );
add_image_size( 'defunti_sidebar', 550, 360, true );
add_image_size( 'banner_half_page', 300, 600, true );
add_image_size( 'copertina_video_tv_home', 950, 520, true );
add_image_size( 'community_book_home', 380, 400, true );
add_image_size( 'banner_sidebar_300_250', 300, 250, true );
add_image_size( 'lista_topics', 350, 210, true );
add_image_size( 'img_gallery_multimedia', 350, 210, true );
add_image_size( 'img_gallery_multimedia_single', 930, 523 );
add_image_size( 'logo_principale', 99999, 100 );






if (!function_exists('eventi_oggi_e_domani')):
function eventi_oggi_e_domani(){
    $today=date('Y/m/j');

    $tomorrow_= new DateTime('tomorrow');
    $tomorrow = $tomorrow_->format('Y/m/j');

    $eventi_oggi_e_domani=get_field('eventi_oggi_e_domani','option');

    $eventi_oggi_e_domani_counter=count($eventi_oggi_e_domani);


    if ($eventi_oggi_e_domani_counter>0):
        foreach ($eventi_oggi_e_domani as $evento):
            $eventi_oggi_e_domani_array_post[]=$evento['evento_oggi_e_domani'];
        endforeach;
    endif;
    $eventi_oggi_e_domani=$eventi_oggi_e_domani_array_post;
    if ($eventi_oggi_e_domani_counter<5):
        $args=array(
            'posts_per_page'   => 5,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'   => 'meta_value', //or 'meta_value_num'
            'meta_key' => 'data',
            'order' => 'ASC',
            'include'          => '',
            'exclude'          => '',
            'post_type'        => 'eventi',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'author_name'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => '',
            'meta_query'	=> array(
                'relation' => 'OR',
                array(
                    'key' => 'data',
                    'value' => $today.' 00:00:00',
                    'compare' => '==',
                    'type' => 'DATETIME'
                ),
                array(
                    'key' => 'data',
                    'value' => $tomorrow.' 23:59:59',
                    'compare' => '==',
                    'type' => 'DATETIME'
                ),
                array(
                    'key' => 'data_e_orario_finali',
                    'value' => $today.' 00:00:00',
                    'compare' => '==',
                    'type' => 'DATETIME'
                ),
                array(
                    'key' => 'data_e_orario_finali',
                    'value' => $tomorrow.' 23:59:59',
                    'compare' => '==',
                    'type' => 'DATETIME'
                ),
                array(

                    'relation' => 'AND',
                    array(
                        'relation' => 'OR',
                        array(
                            'key' => 'data',
                            'value' => $today.' 23:59:59',
                            'compare' => '<=',
                            'type' => 'DATETIME'
                        ),
                        array(
                            'key' => 'data',
                            'value' => $tomorrow.' 23:59:59',
                            'compare' => '<=',
                            'type' => 'DATETIME'
                        ),
                    ),
                    array(
                        'relation' => 'OR',
                        array(
                            'key' => 'data_e_orario_finali',
                            'value' => $today.' 00:00:00',
                            'compare' => '>=',
                            'type' => 'DATETIME'
                        ),
                        array(
                            'key' => 'data_e_orario_finali',
                            'value' => $tomorrow.' 00:00:00',
                            'compare' => '>=',
                            'type' => 'DATETIME'
                        ),
                    ),
                ),
            ),
        );


        $eventi_oggi_e_domani_automatici = get_posts( $args );
    endif;

    if ($eventi_oggi_e_domani_automatici):
        foreach($eventi_oggi_e_domani_automatici as $evento_da_aggiungere):
            $check_data_esclusa=0;
            if ($eventi_oggi_e_domani_counter<5):
                $date_escluse=get_field('date_escluse', $evento_da_aggiungere->ID);
                if ($date_escluse):
                    foreach($date_escluse as $data_esclusa):

                        $today=date('j/m/Y');
                        $tomorrow_= new DateTime('tomorrow');
                        $tomorrow = $tomorrow_->format('j/m/Y');
                        if ($data_esclusa['data_esclusa']==$today || $data_esclusa['data_esclusa']==$tomorrow):
                            $check_data_esclusa=1;
                        endif;
                    endforeach;
                endif;
                if ($check_data_esclusa==0):
                    $eventi_oggi_e_domani[]=$evento_da_aggiungere;
                endif;
                $eventi_oggi_e_domani_counter++;
            endif;
        endforeach;
    endif;

    return $eventi_oggi_e_domani;
}

endif;

if (!function_exists('eventi_futuri')):

function eventi_futuri(){


    $tomorrow_= new DateTime('tomorrow');
    $tomorrow = $tomorrow_->format('Y/m/j H:i:s');

    $eventi_futuri=get_field('eventi_futuri','option');
    $eventi_futuri_counter=count($eventi_futuri);

    if ($eventi_futuri_counter>0):
        if ($eventi_futuri):
            foreach ($eventi_futuri as $evento):
                $eventi_futuri_array_post[]=$evento['evento_futuro'];
            endforeach;
        endif;
    endif;

    $eventi_futuri=$eventi_futuri_array_post;

    if ($eventi_futuri_counter<5):

        $args=array(
            'posts_per_page'   => 5-$eventi_futuri_counter,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'   => 'meta_value', //or 'meta_value_num'
            'meta_key' => 'data',
            'order' => 'ASC',
            'include'          => '',
            'exclude'          => '',
            'post_type'        => 'eventi',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'author_name'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => '',
            'meta_query'	=> array(
                array(
                    'key' => 'data',
                    'value' => $tomorrow,
                    'compare' => '>',
                    'type' => 'DATETIME'
                )
            ),
        );

        $eventi_futuri_automatici = get_posts( $args );
    endif;
    if ($eventi_futuri_automatici):
        foreach($eventi_futuri_automatici as $evento_da_aggiungere):

            $eventi_futuri[]=$evento_da_aggiungere;

        endforeach;
    endif;

    return $eventi_futuri;
}

endif;

function show_date($date){

    $date_arr=explode('/', $date);

    $date_for_strotime= $date_arr[1].'/'.$date_arr[0].'/'.$date_arr[2];

    $dateformatstring = "j F Y";
    $unixtimestamp = strtotime($date_for_strotime);

    $date_to_show=date_i18n($dateformatstring, $unixtimestamp);

    return $date_to_show;

}

require_once ('inc/social_utils/social_functions.php');

//recupero i topics più usati nelle ultime 20 news che saranno mostrati in home in coda a quelli gestiti manualmente
function topics_ultime_20_news(){
    $args_news=array(
        'posts_per_page'   => 20,
        'offset'           => 0,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'news',
        'post_status'      => 'publish',
        'suppress_filters' => false
    );

    $news=get_posts($args_news);


    $array_topics= array();
    foreach ($news as $new):
        $topics=wp_get_post_terms( $new->ID, 'topics' );
        foreach($topics as $topic):
            $array_topics[]=$topic->term_id;
        endforeach;
    endforeach;
    $array_topics_counter=array_count_values($array_topics);
    asort($array_topics_counter);
    $array_topics_counter=array_reverse($array_topics_counter, true);
    return $array_topics_counter;

}
function topics_automatici(array $topics_mostrati){

    $topics_mostrati_counter=count($topics_mostrati);
    $array_topics_counter=topics_ultime_20_news();

    $counter=1;


    foreach ($array_topics_counter as $key => $array_topics_counter_single):
        if ($counter<=6-$topics_mostrati_counter && !in_array($key,$topics_mostrati)):
            $topics_da_mostrare[]=get_term_by( 'term_id', $key, 'topics');
            $counter++;
        endif;
    endforeach;

    return $topics_da_mostrare;
}

function pubblica_contributo_utente($post){
    // controllo se è post type "contributi utenti", se il campo "pubblica come" è valorizzato e se esiste già un post associato al contributo
    if (get_post_type($post)=='contributi-utenti' && get_field('pubblica_come', $post) &&  (!get_post_meta($post, 'post_associato') || (!get_post(get_post_meta($post, 'post_associato')[0])))):
        $content_post = get_post($post);
        $content = $content_post->post_content;
        $my_post = array(
            'post_title'    => get_the_title($post),
            'post_content'  => $content,
            'post_status'   => 'draft',
            'post_author'   => '',
            'post_type'   => get_field('pubblica_come', $post),
            'post_category' => ''
        );



// crea il post associato
        $id_post_creato = wp_insert_post( $my_post );

// verifico se il post d'origine ha una thumbnail
//se si imposto la thumb del nuovo post
        $post_thumbnail_id = get_post_thumbnail_id( $post );
        if ($post_thumbnail_id):
            set_post_thumbnail( $id_post_creato, $post_thumbnail_id );
        endif;

//verifico se il post d'origine ha foto allegate
// se si le associo al nuovo post


    $gallery_post=get_field('contributo_utenti_gallery', $post);

        if ($gallery_post):
            foreach($gallery_post as $img):


                $media_post = wp_update_post( array(
                    'ID'            => $img['ID'],
                    'post_parent'   => $id_post_creato,
                ), true );

            endforeach;

        endif;

// aggiorno i campi che contengono gli id dei due post (contributo e post associato) e il nome utente che ha inserito il contributo
        update_field('contributo_associato', intval($post), $id_post_creato);
        update_field('nome_utente_contributo', get_field('nome_utente', $post), $id_post_creato);
        update_field('post_associato', intval($id_post_creato), $post);

        wp_redirect(get_bloginfo('url').'/wp-admin/post.php?post='.$id_post_creato.'&action=edit&from=cb');

        exit;

    endif;
}

add_action( 'save_post', 'pubblica_contributo_utente' );



//blocco js per check sui quartieri al salvataggio post

add_action('admin_footer', function() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //taxonomy
            var $scope = $('#quartierediv');
            if ($scope.length>0) {
                $('#publish').click(function () {
                    if ($scope.find('input:checked').length > 0) {

                        return true;
                    } else {
                        alert('Selezionare un quartiere per il post!');
                        return false;
                    }
                });
            }else{
                return true;
            }
        });
    </script>
    <?php
});


function sample_admin_notice__success() {
    ?>
    <div class="notice notice-success is-dismissible">
        <p><?php echo 'Questo contenuto è stato creato da un Contributo Utente.';?></p>
    </div>
    <?php
}

if (isset($_GET['from'])):
    $from=$_GET['from'];
    if ($from=='cb'):
        add_action( 'admin_notices', 'sample_admin_notice__success' );
    endif;
endif;


function my_pagination() {

    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){

        $current_page = max(1, get_query_var('paged'));

        echo '<div class="pager">';

        echo paginate_links(array(
            'base' => add_query_arg( 'paged', '%#%' ),
            'format' => '',
            'show_all' => false,
            'end_size' => 1,
            'mid_size' => 2,
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text' => '<span class="prev arrows"><span class="icon icone-icon-arrow-left" aria-hidden="true"></span></span>',
            'next_text' => '<span class="next arrows"><span class="icon icone-icon-arrow-right" aria-hidden="true"></span></span>'
        ));

        echo '</div>';

    }

}

function get_thumb_descritpion($post_id){
    $thumb_description='';
    if ($post_id):
        $imgID  = get_post_thumbnail_id(get_the_ID());
        $img = get_post($imgID);
        $thumb_description=$img->post_content;
    endif;

    return $thumb_description;
}



function intervalli_necrologi($periodo){

    $parametri='';

    if ($periodo=='oggi'):
        $today = date("Y-m-d");
        $parametri='data_1='.$today.'&data_2='.$today.'&periodo=oggi';
    elseif($periodo=='settimana'):
        $d = strtotime("today");
        $start_week = strtotime("monday this week",$d);
        $end_week = strtotime("sunday this week",$d);
        $current_week_start = date("Y-m-d",$start_week);
        $current_week_end = date("Y-m-d",$end_week);
        $parametri='data_1='.$current_week_start.'&data_2='.$current_week_end.'&periodo=settimana';
    elseif($periodo=='mese'):
        $current_month_start = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $current_month_end = date('Y-m-d', mktime(0, 0, 0, date('m')+1, 0, date('Y')));
        $parametri='data_1='.$current_month_start.'&data_2='.$current_month_end.'&periodo=mese';
    elseif($periodo=='anno'):
        $current_year_start = date("Y").'-01-01';
        $current_year_end = date("Y").'-12-31';
        $parametri='data_1='.$current_year_start.'&data_2='.$current_year_end.'&periodo=anno';
    endif;

    return $parametri;


}
function filtri_applicati($periodo){

    $messaggio='';

    if ($periodo=='oggi'):
        $messaggio='Necrologi di oggi';
    elseif($periodo=='settimana'):
        $messaggio='Necrologi della settimana';
    elseif($periodo=='mese'):
        $messaggio='Necrologi del mese';
    elseif($periodo=='anno'):
        $messaggio='Necrologi dell \' anno';
    endif;

    $html_messaggio='<div class="top-title filtri_applicati"><h2 class="filtri_applicati">'.$messaggio.'</h2></div>';

    return $html_messaggio;


}





function wpse_63614_restructure_template_hierarchy(){
    $taxonomy_query=get_query_var('taxonomy');
    if ($taxonomy_query=='quartiere'):
        include( get_template_directory().'/taxonomy-quartiere.php' );
    elseif($taxonomy_query=='categoria'):
        include( get_template_directory().'/taxonomy-categoria.php' );
    else:
        return;
    endif;
    exit;
}
add_action( 'template_redirect', 'wpse_63614_restructure_template_hierarchy' );

// funzioni per il salvataggio da form dei contributi utenti
// salva il post, le gallery di foto inserite, la url video
// salva il post, le gallery di foto inserite, la url video
function salva_contributo_utente(){
    if ( isset( $_POST['sub']) && wp_verify_nonce( $_POST['sub'], 'submit' )) :

        $contenuto_contributo=$_POST['contenuto'];
        $tipologia_contributo=$_POST['tipologia_contenuto'];
        $url_video_contributo=$_POST['video'];


        $form_file=$_FILES;
        $ii=0;
        $utente=wp_get_current_user();
        $my_post = array(
            'post_content' => $contenuto_contributo,
            'post_content_filtered' => '',
            'post_title' => 'Contributo Utente - '.$utente->display_name,
            'post_excerpt' => '',
            'post_status' => 'publish',
            'post_type' => 'contributi-utenti',
            'comment_status' => '',
            'ping_status' => '',
            'post_password' => '',
            'to_ping' =>  '',
            'pinged' => '',
            'post_parent' => 0,
            'menu_order' => 0,
            'guid' => '',
            'import_id' => 0,
            'context' => '',
        );

        $new_post_id=wp_insert_post( $my_post );
        foreach ($form_file as $file) :
            $num=sizeof($file['name']);
            for ($ii=0; $ii<=$num-1; $ii++):
                $array_for_up['elemento']['name']=$file['name'][$ii]['img_gallery_cbu'];
                $array_for_up['elemento']['type']=$file['type'][$ii]['img_gallery_cbu'];
                $array_for_up['elemento']['tmp_name']=$file['tmp_name'][$ii]['img_gallery_cbu'];
                $array_for_up['elemento']['error']=$file['error'][$ii]['img_gallery_cbu'];
                $array_for_up['elemento']['size']=$file['size'][$ii]['img_gallery_cbu'];





                $aless=upload_files($array_for_up, $new_post_id);
                $array_for_acf_gallery[]=$aless;

                //$att=wp_get_attachment_url( $aless);




            endfor;
        endforeach;

        if (is_array($array_for_acf_gallery) && count($array_for_acf_gallery)>0):
            update_field('field_58861451976bb',$array_for_acf_gallery,$new_post_id);
        endif;
        update_field('field_58861490976bd',$tipologia_contributo,$new_post_id);
        update_field('field_58861475976bc',$url_video_contributo,$new_post_id);
        update_field('field_58861d7db5d98',$utente->ID, $new_post_id);
        update_field('field_587cad9a69373',$utente->display_name, $new_post_id);

        $messaggio='Grazie per la tua segnalazione.<br>Il tuo contributo è stato inserito correttamente.';
    endif;

    return $messaggio;

}

function upload_files($nome, $post_id){

    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );
    $attachment_id = my_media_handle_upload( $nome, $post_id );
    return $attachment_id;

}

function my_media_handle_upload($array_for_up, $post_id, $post_data = array(), $overrides = array( 'test_form' => false )) {
    $time = current_time('mysql');
    if ( $post = get_post($post_id) ) {
        if ( substr( $post->post_date, 0, 4 ) > 0 )
            $time = $post->post_date;
    }
    //$name = $_FILES[$file_id]['name'];
    $name = $array_for_up['elemento']['name'];
    $file = wp_handle_upload($array_for_up['elemento'], $overrides, $time);
    if ( isset($file['error']) )
        return new WP_Error( 'upload_error', $file['error'] );
    $name_parts = pathinfo($name);
    $name = trim( substr( $name, 0, -(1 + strlen($name_parts['extension'])) ) );
    $url = $file['url'];
    $type = $file['type'];
    $file = $file['file'];
    $title = $name;
    $content = '';
    $excerpt = '';
    if ( preg_match( '#^audio#', $type ) ) {
        $meta = wp_read_audio_metadata( $file );
        if ( ! empty( $meta['title'] ) ) {
            $title = $meta['title'];
        }
        if ( ! empty( $title ) ) {
            if ( ! empty( $meta['album'] ) && ! empty( $meta['artist'] ) ) {
                /* translators: 1: audio track title, 2: album title, 3: artist name */
                $content .= sprintf( __( '"%1$s" from %2$s by %3$s.' ), $title, $meta['album'], $meta['artist'] );
            } elseif ( ! empty( $meta['album'] ) ) {
                /* translators: 1: audio track title, 2: album title */
                $content .= sprintf( __( '"%1$s" from %2$s.' ), $title, $meta['album'] );
            } elseif ( ! empty( $meta['artist'] ) ) {
                /* translators: 1: audio track title, 2: artist name */
                $content .= sprintf( __( '"%1$s" by %2$s.' ), $title, $meta['artist'] );
            } else {
                $content .= sprintf( __( '"%s".' ), $title );
            }
        } elseif ( ! empty( $meta['album'] ) ) {
            if ( ! empty( $meta['artist'] ) ) {
                /* translators: 1: audio album title, 2: artist name */
                $content .= sprintf( __( '%1$s by %2$s.' ), $meta['album'], $meta['artist'] );
            } else {
                $content .= $meta['album'] . '.';
            }
        } elseif ( ! empty( $meta['artist'] ) ) {
            $content .= $meta['artist'] . '.';
        }
        if ( ! empty( $meta['year'] ) )
            $content .= ' ' . sprintf( __( 'Released: %d.' ), $meta['year'] );
        if ( ! empty( $meta['track_number'] ) ) {
            $track_number = explode( '/', $meta['track_number'] );
            if ( isset( $track_number[1] ) )
                $content .= ' ' . sprintf( __( 'Track %1$s of %2$s.' ), number_format_i18n( $track_number[0] ), number_format_i18n( $track_number[1] ) );
            else
                $content .= ' ' . sprintf( __( 'Track %1$s.' ), number_format_i18n( $track_number[0] ) );
        }
        if ( ! empty( $meta['genre'] ) )
            $content .= ' ' . sprintf( __( 'Genre: %s.' ), $meta['genre'] );
        // Use image exif/iptc data for title and caption defaults if possible.
    } elseif ( 0 === strpos( $type, 'image/' ) && $image_meta = @wp_read_image_metadata( $file ) ) {
        if ( trim( $image_meta['title'] ) && ! is_numeric( sanitize_title( $image_meta['title'] ) ) ) {
            $title = $image_meta['title'];
        }
        if ( trim( $image_meta['caption'] ) ) {
            $excerpt = $image_meta['caption'];
        }
    }
    // Construct the attachment array
    $attachment = array_merge( array(
        'post_mime_type' => $type,
        'guid' => $url,
        'post_parent' => $post_id,
        'post_title' => $title,
        'post_content' => $content,
        'post_excerpt' => $excerpt,
    ), $post_data );
    // This should never be set as it would then overwrite an existing attachment.
    unset( $attachment['ID'] );
    // Save the data
    $id = wp_insert_attachment($attachment, $file, $post_id);
    if ( !is_wp_error($id) ) {
        wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );
    }
    return $id;
}


function acf_set_readonly($field) {
    if($field['_name'] == 'contributo_utente_id_utente')
        $field['readonly'] = 1;
    return $field;
}
add_filter('acf/load_field', 'acf_set_readonly');

wp_enqueue_script( 'jquery' );




function get_quartiere($id_post){
    $quartiere=wp_get_post_terms($id_post,'quartiere');
    return $quartiere;
}



function mytheme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>

    <div class="post-content">
        <div class="row row-margin-0 margin-bottom-20">
            <div class="col-md-2 avatar_container">
                <?php echo get_avatar($comment,$size='',$default='','', array('class' => array('avatar_comment_user')) ); ?>
            </div>
            <div class="col-md-10 content_defunto">
                <p class="user_name">
                    <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()) ?>
                </p>
                <p class="comment_date">
                    <?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>
                </p>
                <p class="comment_text"><?php echo  get_comment_text() ?></p>
            </div>
        </div>
    </div>


    <?php
}


function geocode($address){

    // url encode the address
    $address = urlencode($address);

    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

    // get the json response
    $resp_json = file_get_contents($url);

    // decode the json
    $resp = json_decode($resp_json, true);

    // response status will be 'OK', if able to geocode given address
    if($resp['status']=='OK'){

        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];

        // verify if data is complete
        if($lati && $longi && $formatted_address){

            // put the data in the array
            $data_arr = array();

            array_push(
                $data_arr,
                $lati,
                $longi,
                $formatted_address
            );

            return $data_arr;

        }else{
            return false;
        }

    }else{
        return false;
    }
}


function filtro_data_da_mostrare($filtro_data){
        $data_1_array=explode("-",$filtro_data);
        if ($data_1_array[0] && $data_1_array[1] && $data_1_array[2]):
            $data_filtro_to_show=' - '.show_date($data_1_array[2].'/'.$data_1_array[1].'/'.$data_1_array[0]);
        endif;

        return $data_filtro_to_show;
}


function autore(){
    $fname = get_the_author_meta('first_name');
    $lname = get_the_author_meta('last_name');

    if ($fname && $lname):
        $author=$fname.' '.$lname;
    else:
        $author=get_the_author();
    endif;
return $author;
}



function is_video($post_id){

    $terms = wp_get_post_terms( $post_id, 'categoria_multimedia' );
    $categoria_multimedia=array();
    foreach($terms as $term):
        $categoria_multimedia[]=$term->term_id;
    endforeach;
    if (in_array(get_id_of_multimedia_cat_img(), $categoria_multimedia)):
        $check=false;
    elseif(in_array(get_id_of_multimedia_cat_video(), $categoria_multimedia)):
        $check=true;
    endif;
    return $check;
}

function is_immagine($post_id){

    $terms = wp_get_post_terms( $post_id, 'categoria_multimedia' );
    $categoria_multimedia=array();
    foreach($terms as $term):
        $categoria_multimedia[]=$term->term_id;
    endforeach;
    if (in_array(get_id_of_multimedia_cat_img(), $categoria_multimedia)):
        $check=true;
    elseif(in_array(get_id_of_multimedia_cat_img(), $categoria_multimedia)):
        $check=false;
    endif;
    return $check;
}

function get_random_defunto (){

    $args = array(
        'posts_per_page'   => 10,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'defunti',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	   => '',
        'author_name'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );
    $defunti = get_posts( $args );

    $numero_defunti=count($defunti);

    return $defunti[rand(0,$numero_defunti-1)];
}


