<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 23/01/17
 * Time: 11.11
 * Template Name: Coupon
 */


get_header(); ?>


		<?php
		while ( have_posts() ) : the_post();
        ?>
      <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

          <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">

              <?php get_template_part('template-parts/content', 'sharing');?>

            <div class="post-content red_link">
                <?php if (is_user_logged_in()):?>

                <h1><?php echo get_the_title();?></h1>
                <?php if (has_post_thumbnail()):?>
                    <figure>
                        <?php $thumb_description=get_thumb_descritpion(get_the_ID());?>
                        <?php $default_attr = array('class'	=> "img-responsive",'alt'	=> $thumb_description);?>
                        <?php the_post_thumbnail('', $default_attr);?>
                        <figcaption><?php echo $thumb_description;?></figcaption>
                    </figure>
                <?php endif;?>
                <?php the_content();?>
                <?php else:?>
                    <h1><?php echo get_the_title();?></h1>
                    <h2>Per visualizzare questa pagina devi essere loggato. Accedi oppure iscriviti al sito.</h2>
                    <hr class="hr_login">
                    <?php get_template_part('template-parts/content', 'social-login');?>
                <?php endif;?>
            </div>




          </div><!-- /col-md-8 -->

            <?php get_sidebar();?>


        </div><!-- /row -->

      </main>

		<?php endwhile; // End of the loop.
		?>
    </div><!-- /container -->
<?php
get_footer();
