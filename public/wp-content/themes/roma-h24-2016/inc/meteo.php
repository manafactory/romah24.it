<?php


function roma_h24_get_meteo(){

/**
 * Meteo Roma
 */

    $weather = wp_cache_get( 'roma_h24_get_meteo' );
    if ( false === $weather ) {

        $key = "4d8c05eef30940cf8e5122242172001";
        $city = 'rome';
        $url = "http://api.apixu.com/v1/current.json?key=$key&q=$city&=" ;

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        $json_output=curl_exec($ch);
        $weather = json_decode($json_output);

        wp_cache_set( 'roma_h24_get_meteo', $weather );
    }

    echo '<span class="icon" aria-hidden="true"><img style="width: 50px; height:50px;" src="'.$weather->current->condition->icon.'" /></span><p>'.$weather->current->temp_c.'°</p>';
}