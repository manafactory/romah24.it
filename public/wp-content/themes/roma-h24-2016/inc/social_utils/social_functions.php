<?php


//Get Facebook Likes Count of a page
function fbLikeCount(){
    $id=ID_FACEBOOK;
    $appid=APP_ID_FACEBOOK;
    $appsecret=APP_SECRET_FACEBOOK;
    //Construct a Facebook URL
    $json_url ='https://graph.facebook.com/'.$id.'?access_token='.$appid.'|'.$appsecret.'&fields=fan_count';

    $json = file_get_contents($json_url);

    $json_output = json_decode($json);
    //Extract the likes count from the JSON object
    if($json_output->fan_count){
        return $likes = $json_output->fan_count;
    }else{
        return 0;
    }
}


function twitter_followers(){
    require_once('TwitterAPIExchange.php');

    /** Set access tokens here - see: https://dev.twitter.com/apps/ **/
    $settings = array(
        'oauth_access_token' => OATH_ACCESS_TOKEN,
        'oauth_access_token_secret' => OATH_ACCESS_TOKEN_SECRET,
        'consumer_key' => CONSUMER_KEY,
        'consumer_secret' => CONSUMER_SECRET
    );

    $ta_url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $getfield = '?screen_name=Typi_';
    $requestMethod = 'GET';
    $twitter = new TwitterAPIExchangee($settings);
    $follow_count=$twitter->setGetfield($getfield)
        ->buildOauth($ta_url, $requestMethod)
        ->performRequest();
    $data = json_decode($follow_count, true);
    $followers_count=$data[0]['user']['followers_count'];
    return $followers_count;
}


function insta_follower(){
    $user_id = USER_ID;
    $access_token = ACCESS_TOKEN;
    $api_insta = 'https://api.instagram.com/v1/users/'.$user_id.'/?access_token='.$access_token;
    $insta_json=file_get_contents($api_insta);
    $data = json_decode($insta_json, true);
    return $data['data']['counts']['followed_by'];

}


