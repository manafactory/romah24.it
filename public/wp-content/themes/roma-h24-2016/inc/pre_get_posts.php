<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 02/02/17
 * Time: 15.38
 */




function filtra_post_per_giorno($query){


    if(is_main_query()==false || is_admin() || !isset($_GET['data_1']) || $_GET['filtra_news']!=1) {
        return $query;
    }else {
        $data_1 = $_GET['data_1'];
        $data_2 = $_GET['data_1'];

        $data_inizio_a = explode(' ', $data_1);
        $data_inizio = $data_inizio_a[0];
        $data_fine_a = explode(' ', $data_2);
        $data_fine = $data_fine_a[0];
        if (get_query_var('post_type') == 'eventi' || get_query_var('categoria_eventi')):

            $query->set('meta_query', array(
                array(
                    'key' => 'data',
                    'value' => array($data_inizio, $data_fine),
                    'compare' => 'BETWEEN',
                    'type' => 'DATE',
                    'inclusive' => true,
                )
            ));

        else:
            $query->set('date_query', array(array('after' => $data_inizio . ' 00:00:00', 'before' => $data_fine . ' 23:59:59', 'inclusive' => true)));
        endif;

        return $query;
    }
}

add_action('pre_get_posts', 'filtra_post_per_giorno');




function get_post_of_quartiere($query){
    if(!is_main_query())
        return $query;
    if(is_admin())
        return $query;


    if ($query->is_tax('quartiere')):

        if (isset($_GET['post_type'])):
            $post_type=$_GET['post_type'];
        else:
            $post_type='news';
        endif;
        $query->set('post_type', $post_type);
    endif;
}

add_action('pre_get_posts', 'get_post_of_quartiere');


function get_post_of_categoria_news($query){
    if(!is_main_query())
        return $query;
    if(is_admin())
        return $query;

    if ($query->is_tax('categoria')):

        if (isset($_GET['post_type'])):
            $post_type=$_GET['post_type'];
        else:
            $post_type='news';
        endif;
        $query->set('post_type', $post_type);
    endif;
}

add_action('pre_get_posts', 'get_post_of_categoria_news');


// pre_get_post per ricerca persona cara tra i defunti



    function get_persona_cara($query){
        if(!is_main_query())
            return $query;
        if(is_admin())
            return $query;

        if (isset($_GET['nome_persona'])&& wp_verify_nonce( $_POST['cerca_defunto'], 'submit' )):
        $persona_cara=$_GET['nome_persona'];

        $query->set('s', $persona_cara);
        endif;
        return $query;

    }
if (isset($_GET['nome_persona'])&& wp_verify_nonce( $_POST['cerca_defunto'], 'submit' )):
    add_action('pre_get_posts', 'get_persona_cara');

endif;




    function get_necrologi_nel_tempo($query){
        if(!is_main_query())
            return $query;
        if(is_admin())
            return $query;

        if (isset($_GET['data_1'])&&isset($_GET['data_2'])):
            $data_1=$_GET['data_1'];
            $data_2=$_GET['data_2'];

            $data_inizio_a=explode(' ',$data_1);
            $data_inizio=$data_inizio_a[0];
            $data_fine_a=explode(' ',$data_2);
            $data_fine=$data_fine_a[0];

            $query->set('date_query', array( array('after' => $data_inizio,'before' => $data_fine,'inclusive' => true)));
        endif;
        return $query;

    }
if (isset($_GET['data_1'])&&isset($_GET['data_2'])):
    add_action('pre_get_posts', 'get_necrologi_nel_tempo');
endif;