<?php

add_action( 'init', 'register_custom_post_type_function');
function register_custom_post_type_function()
{


    register_post_type('news', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'News', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'News', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutte le News', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuova News', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica News', /*  Testo per modifica */
            'new_item' => 'Nuova News', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza News', /* Testo per visualizzare */
            'search_items' => 'Cerca News', /* Testo per la ricerca*/
            'not_found' => 'Nessuna News trovata', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessuna News trovata nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'News Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-media-document', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */






    $labels = array(
        'name' => __('Categorie'),
        'singular_name' => __('Categoria'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria'),
    );

    register_taxonomy('categoria',  array('news', 'multimedia'), $args);



    $labels = array(
        'name' => __('Zone'),
        'singular_name' => __('Zona'),
        'search_items' => __('Ricerca Zona'),
        'popular_items' => __('Zone popolari'),
        'all_items' => __('Tutti le Zone'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Zona'),
        'update_item' => __('Aggiorna Zona'),
        'add_new_item' => __('Aggiungi nuovo Zona'),
        'new_item_name' => __('Nuovo Nome Zona'),
        'separate_items_with_commas' => __('Separa le Zone da virgola'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Zona'),
        'choose_from_most_used' => __('Scegli tra le Zone più usati'),
        'not_found' => __('Nessuna Zona trovata.'),
        'menu_name' => __('Zone'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'zone'),
    );
    $array_post_type_for_zone= array('news', 'speciali','defunti', 'eventi', 'multimedia','community-book');
    
    if (post_type_exists('itinerari')):
        $array_post_type_for_zone[]='itinerari';
    endif;
    register_taxonomy('quartiere', $array_post_type_for_zone, $args);


    $labels = array(
        'name' => __('Topics'),
        'singular_name' => __('Topic'),
        'search_items' => __('Ricerca Topics'),
        'popular_items' => __('Topics popolari'),
        'all_items' => __('Tutti i Topics'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Topic'),
        'update_item' => __('Aggiorna Topic'),
        'add_new_item' => __('Aggiungi nuovo Topic'),
        'new_item_name' => __('Nuovo Nome Topic'),
        'separate_items_with_commas' => __('Separa i Topics da virgola'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Topic'),
        'choose_from_most_used' => __('Scegli tra i Topics più usati'),
        'not_found' => __('Nessun Topic trovato.'),
        'menu_name' => __('Topics'),
    );

    $args = array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'topics'),
    );

    register_taxonomy('topics', array('news', 'multimedia'), $args);



    register_post_type('speciali', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Speciali', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Speciale', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti gli Speciali', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Speciale', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Speciale', /*  Testo per modifica */
            'new_item' => 'Nuovo Speciale', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Speciali', /* Testo per visualizzare */
            'search_items' => 'Cerca Speciali', /* Testo per la ricerca*/
            'not_found' => 'Nessuno Speciale trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessuno Speciali trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Speciali Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-star-empty', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */


    $labels = array(
        'name' => __('Categorie Speciali'),
        'singular_name' => __('Categoria Speciali'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie Speciali'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria_speciali'),
    );

    register_taxonomy('categoria_speciali', 'speciali', $args);

    register_post_type('annunci', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Annunci', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Annuncio', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti gli Annunci', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Annuncio', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Annuncio', /*  Testo per modifica */
            'new_item' => 'Nuovo Annuncio', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Annunci', /* Testo per visualizzare */
            'search_items' => 'Cerca Annunci', /* Testo per la ricerca*/
            'not_found' => 'Nessuno Annuncio trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessuno Annuncio trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Annunci Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-sticky', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */






    $labels = array(
        'name' => __('Categorie Annunci'),
        'singular_name' => __('Categoria Annunci'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie Annunci'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria_annunci'),
    );

    register_taxonomy('categoria_annunci', 'annunci', $args);


    register_post_type('eventi', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Eventi', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Evento', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti gli Eventi', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Evento', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Evento', /*  Testo per modifica */
            'new_item' => 'Nuovo Evento', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Evento', /* Testo per visualizzare */
            'search_items' => 'Cerca Evento', /* Testo per la ricerca*/
            'not_found' => 'Nessuno Evento trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessuno Evento trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Eventi Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-calendar-alt', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */

    $labels = array(
        'name' => __('Categorie Eventi'),
        'singular_name' => __('Categoria Eventi'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie Eventi'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria_eventi'),
    );

    register_taxonomy('categoria_eventi', 'eventi', $args);


    register_post_type('multimedia', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Multimedia', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Multimedia', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti i Multimedia', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Multimedia', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Multimedia', /*  Testo per modifica */
            'new_item' => 'Nuovo Multimedia', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Multimedia', /* Testo per visualizzare */
            'search_items' => 'Cerca Multimedia', /* Testo per la ricerca*/
            'not_found' => 'Nessuno Multimedia trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessuno Multimedia trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Multimedia Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-controls-play', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */



    $labels = array(
        'name' => __('Categorie Multimedia'),
        'singular_name' => __('Categoria Multimedia'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie Multimedia'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria_multimedia'),
    );

    register_taxonomy('categoria_multimedia', 'multimedia', $args);



    register_post_type('defunti', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Defunti', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Defunto', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti i Defunti', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Defunto', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Defunto', /*  Testo per modifica */
            'new_item' => 'Nuovo Defunto', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Defunto', /* Testo per visualizzare */
            'search_items' => 'Cerca Defunto', /* Testo per la ricerca*/
            'not_found' => 'Nessun Defunto trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessun Defunto trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Defunti Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-minus', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */



    register_post_type('community-book', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Community Book', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Community Book', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti i Community Book', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Community Book', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Community Book', /*  Testo per modifica */
            'new_item' => 'Nuovo Community Book', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Community Book', /* Testo per visualizzare */
            'search_items' => 'Cerca Community Book', /* Testo per la ricerca*/
            'not_found' => 'Nessun Community Book trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessun Community Book trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Community Book Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-share-alt2', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */




    $labels = array(
        'name' => __('Categorie Community Book'),
        'singular_name' => __('Categoria Community Book'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie Community Book'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria_community_book'),
    );

    register_taxonomy('categoria_community_book', 'community-book', $args);



    register_post_type('contributi-utenti', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Contributi Utenti', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Contributo', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti i Contributi', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo Contributo', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Contributo', /*  Testo per modifica */
            'new_item' => 'Nuovo Contributo', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Contributo', /* Testo per visualizzare */
            'search_items' => 'Cerca Contributo', /* Testo per la ricerca*/
            'not_found' => 'Nessun Contributo trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessun Contributo trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Contributi Roma H24', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '50', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-groups', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */


}