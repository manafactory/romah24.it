<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 19/01/17
 * Time: 16.21
 */
get_header(); ?>

    <div class="top-title">
        <h1><?php echo (get_queried_object()->name);?></h1>

        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

    </div>

    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->

      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">
            <?php if (have_posts()):?>
              <?php while(have_posts()): the_post();?>

                <div class="box box-article-rounded-thumb">

                  <article>

                    <div class="article-wrapper">
                      <div class="thumb">
                          <?php if (has_post_thumbnail()):?>
                              <?php the_post_thumbnail();?>
                          <?php else:?>
                          <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/placeholder-350x210.jpg">
                          <?php endif;?>
                      </div>
                      <div class="article-content">
                          <?php $gmt_timestamp = get_post_time('G:i', true);?>


                          <h2><a href="<?php echo get_the_permalink();?>"><?php echo $gmt_timestamp;?> | <?php echo get_the_title();?></a> - <a class="link-color" href="<?php echo get_the_permalink();?>">FOTO</a></h2>


                      </div><!-- /article-content -->

                    </div><!-- /article-wrapper -->

                  </article>

                </div><!-- /box-article-rounded-thumb -->
              <?php endwhile;?>
            <?php else:?>
                <?php get_template_part('template-parts/content', 'nessun_contenuto');?>
            <?php endif;?>
          </div><!-- /col-md-8 -->


        <?php get_sidebar('topics');?>
        </div><!-- /row -->
        <div class="row row-margin-0 margin-bottom-20">
            <div class="col-md-8">
                <div class="pagination_container">
                    <?php my_pagination();?>
                </div>

             </div>
        </div>
      </main>

    </div><!-- /container -->

    </div><!-- /container -->
<?php
get_footer();
