<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Roma_H24_2016
 */

get_header(); ?>


		<?php
		while ( have_posts() ) : the_post();
        ?>


      <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

          <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">


            <div class="post-content red_link">

                <?php if (has_post_thumbnail()):?>
                    <figure>
                        <?php $thumb_description=get_thumb_descritpion(get_the_ID());?>
                        <?php $default_attr = array('class'	=> "img-responsive",'alt'	=> $thumb_description);?>
                        <?php the_post_thumbnail('', $default_attr);?>
                        <figcaption><?php echo $thumb_description;?></figcaption>
                    </figure>
                <?php endif;?>
                <?php
                $categoria_news=wp_get_post_terms(get_the_ID(),'categoria_eventi');
                $quartiere=wp_get_post_terms(get_the_ID(),'quartiere');
                ?>
                <h3 class="news_dettaglio_data_e_ora">
                    <?php if ($quartiere):
                        echo '<a href="'.get_term_link($quartiere[0]->term_id).'" title="'.$quartiere[0]->name.'">';
                        echo $quartiere[0]->name;
                        echo '</a>';
                    endif;?>

                    <?php if ($categoria_news):
                        echo ' | <a href="'.get_term_link($categoria_news[0]->term_id).'" title="'.$categoria_news[0]->name.'">';
                        echo $categoria_news[0]->name;
                        echo '</a>';
                    endif;?>
                </h3>
                <h1 class="news-title">
                    <?php the_title();?>
                </h1>

            </div>
              <?php get_template_part('template-parts/content', 'sharing');?>
            <div class="post-content red_link">
                <?php the_content();?>



                <?php
                $speciali_correlati=get_field('speciale_correlato');
                $multimedia_correlati=get_field('multimedia_correlato');
                if ($speciali_correlati || $multimedia_correlati):
                    ?>

                    <div class="correlati_post">
                        <div class="section-title section-title-red">

                            <h3>Leggi anche</h3>

                        </div><!-- /section-title -->
                        <?php

                        if ($speciali_correlati):?>

                            <?php foreach ($speciali_correlati as $speciale):?>
                                <div class="box box-article-small">

                                    <article>

                                        <div class="article-wrapper">

                                            <div class="article-content">

                                                <h2><a href="<?php echo get_the_permalink($speciale->ID)?>">> <?php echo get_the_title($speciale->ID)?></a></h2>

                                            </div><!-- /article-content -->

                                        </div><!-- /article-wrapper -->

                                    </article>

                                </div><!-- /box-article-small -->


                            <?php endforeach;
                        endif;?>
                        <?php
                        if ($multimedia_correlati):?>

                            <?php foreach ($multimedia_correlati as $multimedia):?>
                                <div class="box box-article-small">

                                    <article>

                                        <div class="article-wrapper">

                                            <div class="article-content">

                                                <h2><a href="<?php echo get_the_permalink($multimedia->ID)?>">> <?php echo get_the_title($multimedia->ID)?></a></h2>

                                            </div><!-- /article-content -->

                                        </div><!-- /article-wrapper -->

                                    </article>

                                </div><!-- /box-article-small -->


                            <?php endforeach;
                        endif;?>
                    </div>
                <?php endif;?>
            </div>





          </div><!-- /col-md-8 -->

            <?php get_sidebar('eventi');?>


        </div><!-- /row -->

      </main>

		<?php endwhile; // End of the loop.
		?>
    </div><!-- /container -->
<?php
get_footer();
