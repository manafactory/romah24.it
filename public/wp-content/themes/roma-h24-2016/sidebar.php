<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Roma_H24_2016
 */
if (is_post_type_archive('defunti')):
    $sidebar_id=4;
elseif (is_post_type_archive('news')):
    $sidebar_id=3;

elseif (is_post_type_archive('eventi') || is_tax('categoria_eventi')):
    $sidebar_id=6;

elseif (is_singular('speciali')):
    $sidebar_id=7;
elseif (is_home()):
    $sidebar_id=1;
else:
    $sidebar_id=2;
endif;

if ( ! is_active_sidebar( 'sidebar-'.$sidebar_id ) ) {
	return;
}
?>
<div class="col-md-4">
    <aside class="sidebar">
            <?php dynamic_sidebar( 'sidebar-'.$sidebar_id ); ?>
    </aside>
</div>
