<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Roma_H24_2016
 */

get_header(); ?>


<?php
while ( have_posts() ) : the_post();
    ?>
    <div class="top-title">
        <p class="h1"><?php echo get_the_title();?></p>
        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>
    </div>


    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->


    <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">
            <?php
            if (is_immagine(get_the_ID())):?>
                <?php get_template_part('template-parts/content', 'gallery');?>
            <?php elseif(is_video(get_the_ID())):?>
                <?php get_template_part('template-parts/content', 'video');?>
            <?php endif;?>


        </div><!-- /row -->

    </main>

<?php endwhile; // End of the loop.
?>
    </div><!-- /container -->
<?php
get_footer();

?>


