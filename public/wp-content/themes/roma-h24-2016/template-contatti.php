<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 23/01/17
 * Time: 11.11
 * Template Name: Contattaci
 */

?>
<?php acf_form_head(); ?>
<?php

get_header(); ?>

<?php $messaggio=salva_contributo_utente();?>


<?php

while ( have_posts() ) : the_post();
    ?>
    <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

    </div>


    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->


    <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

            <div class="col-md-8">

            <?php if ($messaggio):?>
                <div class="post-content">

                    <p class="messaggio_form"><?php echo $messaggio;?></p>

                </div>
            <?php endif;?>
                <div class="post-content red_link">

                    <h1><?php echo get_the_title();?></h1>
                    <?php the_content();?>
                    <?php $form=get_field('form_codice');?>
                    <?php if ($form):?>
                    <div>
                        <?php echo do_shortcode( $form ); ?>
                    </div>
                    <?php endif;?>
                </div>


            </div><!-- /col-md-8 -->
            <?php get_sidebar();?>




        </div><!-- /row -->

    </main>

<?php endwhile; // End of the loop.
?>
    </div><!-- /container -->
<?php
get_footer();


?>

<script src="<?php echo get_bloginfo('template_url')?>/assets/jquery.repeater-master/jquery.repeater.js"></script>
<script>
    $(document).ready(function () {
        'use strict';

        $('.repeater').repeater({
            defaultValues: {
                'file': 'foo'

            },
            show: function () {
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },
            ready: function (setIndexes) {

            }
        });

        window.outerRepeater = $('.outer-repeater').repeater({
            isFirstItemUndeletable: true,
            defaultValues: { 'text-input': 'outer-default' },
            show: function () {
                console.log('outer show');
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                console.log('outer delete');
                $(this).slideUp(deleteElement);
            },
            repeaters: [{
                isFirstItemUndeletable: true,
                selector: '.inner-repeater',
                defaultValues: { 'inner-text-input': 'inner-default' },
                show: function () {
                    console.log('inner show');
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    console.log('inner delete');
                    $(this).slideUp(deleteElement);
                }
            }]
        });
    });
</script>
