<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 23/01/17
 * Time: 11.11
 * Template Name: Form Contributo Utenti
 */

?>
<?php

get_header(); ?>

<?php $messaggio=salva_contributo_utente();?>


<?php

while ( have_posts() ) : the_post();
    ?>
    <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

    </div>


    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->


    <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

            <div class="col-md-8">

            <?php if ($messaggio):?>
                <div class="post-content">

                    <p class="messaggio_form"><?php echo $messaggio;?></p>

                </div>
            <?php endif;?>
                <div class="post-content red_link">

                    <?php if (is_user_logged_in()):?>

                        <h1><?php echo get_the_title();?></h1>
                        <?php the_content();?>
                        <div>
                            <form class="form_contributo_utenti repeater" method="post" enctype="multipart/form-data">
                                <?php wp_nonce_field( 'submit', 'sub' ); ?>
                                <div>
                                    <p class="cbutente">Inserisci il tuo contributo *</p>
                                    <textarea name="contenuto" class="form-control cbutente" rows="10" required="required"></textarea>
                                </div>
                                <div>
                                    <p class="cbutente">Seleziona un'opzione per il contributo *</p>

                                    <select class="form-control cbutente" name="tipologia_contenuto" required="required">
                                        <option value="">Seleziona...</option>
                                        <option value="news">News</option>
                                        <option value="speciali">Speciali</option>
                                        <option value="annunci">Annunci</option>
                                        <option value="eventi">Eventi</option>
                                        <option value="multimedia">Multimedia</option>
                                        <option value="defunti">Defunti</option>
                                        <option value="community-book">Community Book</option>
                                        <option value="altro">Altro</option>
                                    </select>
                                </div>
                                <div data-repeater-list="group-a">
                                    <p class="cbutente">Aggiungi una foto</p>
                                    <div data-repeater-item style="margin-top: 0px">

                                        <input class="form-control cbutente repeater_img input_file" type="file" name="img_gallery_cbu" value="B"/>


                                        <input data-repeater-delete type="button" class="repeater_item_delete repeater_img"  value="Elimina"/>
                                    </div>
                                </div>
                                <input data-repeater-create type="button" class="repeater_item_add repeater_img" value="Aggiungi un'altra foto"/>
                                <div>
                                    <p class="cbutente">Inserisci la url di un video</p>
                                    <input name="video" type="url" class="form-control cbutente">
                                </div>
                                <div>
                                    <input class="form-control cbutente" type="submit" value="Invia">
                                </div>
                            </form>
                        </div>
                    <?php else:?>
                        <h1><?php echo get_the_title();?></h1>
                        <h2>Per inserire il tuo contributo devi essere loggato al sito. Accedi oppure iscriviti al sito.</h2>
                        <hr class="hr_login">
                        <?php get_template_part('template-parts/content', 'social-login');?>
                    <?php endif;?>
                </div>


            </div><!-- /col-md-8 -->

            <?php get_sidebar();?>


        </div><!-- /row -->

    </main>

<?php endwhile; // End of the loop.
?>
    </div><!-- /container -->
<?php
get_footer();


?>

<script src="<?php echo get_bloginfo('template_url')?>/assets/jquery.repeater-master/jquery.repeater.js"></script>
<script>
    jQuery(document).ready(function () {
        'use strict';

        jQuery('.repeater').repeater({
            defaultValues: {
                'file': 'foo'

            },
            show: function () {
                jQuery(this).slideDown();
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    jQuery(this).slideUp(deleteElement);
                }
            },
            ready: function (setIndexes) {

            }
        });

        window.outerRepeater = jQuery('.outer-repeater').repeater({
            isFirstItemUndeletable: true,
            defaultValues: { 'text-input': 'outer-default' },
            show: function () {
                console.log('outer show');
                jQuery(this).slideDown();
            },
            hide: function (deleteElement) {
                console.log('outer delete');
                jQuery(this).slideUp(deleteElement);
            },
            repeaters: [{
                isFirstItemUndeletable: true,
                selector: '.inner-repeater',
                defaultValues: { 'inner-text-input': 'inner-default' },
                show: function () {
                    console.log('inner show');
                    jQuery(this).slideDown();
                },
                hide: function (deleteElement) {
                    console.log('inner delete');
                    jQuery(this).slideUp(deleteElement);
                }
            }]
        });
    });
</script>
