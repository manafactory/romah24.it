<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Roma_H24_2016
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<?php if (have_comments()):?>
            <div class="utilities">
                <div class="utilities-left">
                    <p><?php echo number_format_i18n( get_comments_number() );?> Ricordi</p>
                </div><!-- /utilities-left -->
                <div class="utilities-right">
                    <?php $acrgs = array(
                        'base' => add_query_arg( 'cpage', '%#%' ),
                        'format' => '',
                        'total' => $max_page,
                        'current' => $page,
                        'echo' => true,
                        'add_fragment' => '#comments'

                    );?>
                    <?php paginate_comments_links( $args ) ?>
                </div><!-- /utilities-left -->
            </div><!-- /utilities -->

            <?php wp_list_comments('type=comment&callback=mytheme_comment');?>


    <?php $args = array(
        'id_form'           => 'commentform',
        'class_form'      => 'comment-form',
        'id_submit'         => 'submit',
        'class_submit'      => 'form-control cbutente',
        'name_submit'       => 'submit',
        'title_reply'       => __( 'Invia un commento' ),
        'title_reply_to'    => __( 'Leave a Reply to %s' ),
        'cancel_reply_link' => __( 'Cancel Reply' ),
        'label_submit'      => __( 'Commenta' ),
        'format'            => 'xhtml',
        'fields' =>array(
        'author' =>
            '<p style="margin-top: 20px"><label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
            ( $req ? '<span class="required">*</span>' : '' ) .
            '<input id="author" name="author" class="form-control cbutente" type="text" value="" size="30"' . $aria_req . ' /></p>',

        'email' =>
            '<p  style="margin-top: 20px"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
            ( $req ? '<span class="required">*</span>' : '' ) .
            '<input id="email" name="email" class="form-control cbutente" type="text" value="" size="30"' . $aria_req . ' /></p>',
        ),

        'comment_field' =>  '<textarea id="comment" name="comment" class="form-control cbutente" rows="8" aria-required="true">' .
            '</textarea>',


        'logged_in_as' => '<p class="logged-in-as">' .
            sprintf(
                __( 'Sei loggato come  <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
                admin_url( 'profile.php' ),
                $user_identity,
                wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
            ) . '</p>',



        'comment_notes_after' => ''


    );?>

    <div class="row row-margin-0 margin-bottom-20">
        <div class="col-md-12">
            <?php comment_form( $args, $post_id ); ?>
        </div><!-- /col-md-8 -->
    </div><!-- /row -->



<?php endif;?>


