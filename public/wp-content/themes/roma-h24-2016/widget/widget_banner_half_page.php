<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_banner_half_page extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_banner_half_page',
            __('Banner Half Page', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Banner Half Page', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        $banner_half_page=get_field('half_page_300_600','option');
        $banner_half_page_link=get_field('half_page_300_600_link','option');
        if ($banner_half_page && $banner_half_page_link):
?>

        <div class="adv adv-halfpage">

            <a href="<?php echo $banner_half_page_link;?>"><img src="<?php echo $banner_half_page['sizes']['banner_half_page'];?>"></a>

        </div><!-- /adv -->
            <?php
            endif;



    }

// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


}