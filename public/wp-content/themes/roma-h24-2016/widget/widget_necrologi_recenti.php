<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_necrologi_recenti extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_necrologi_recenti',
            __('Necrologi Recenti', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box dei Necrologi recenti', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        ?>

        <div class="section-title section-title-red">

            <h3>Necrologi Recenti</h3>

        </div><!-- /section-title -->

        <div class="box box-article-small">

            <article>

                <div class="article-wrapper">

                    <div class="article-content">

                        <h2><a href="<?php bloginfo('url');?>/defunti/?<?php echo intervalli_necrologi('oggi');?>">> Necrologi di Oggi</a></h2>

                    </div><!-- /article-content -->

                </div><!-- /article-wrapper -->

            </article>

        </div><!-- /box-article-small -->

        <div class="box box-article-small">

            <article>

                <div class="article-wrapper">

                    <div class="article-content">

                        <h2><a href="<?php bloginfo('url');?>/defunti/?<?php echo intervalli_necrologi('settimana');?>">> Necrologi della settimana</a></h2>

                    </div><!-- /article-content -->

                </div><!-- /article-wrapper -->

            </article>

        </div><!-- /box-article-small -->

        <div class="box box-article-small">

            <article>

                <div class="article-wrapper">

                    <div class="article-content">

                        <h2><a href="<?php bloginfo('url');?>/defunti/?<?php echo intervalli_necrologi('mese');?>">> Necrologi del mese</a></h2>

                    </div><!-- /article-content -->

                </div><!-- /article-wrapper -->

            </article>

        </div><!-- /box-article-small -->

        <div class="box box-article-small">

            <article>

                <div class="article-wrapper">

                    <div class="article-content">

                        <h2><a href="<?php bloginfo('url');?>/defunti/?<?php echo intervalli_necrologi('anno');?>">> Necrologi dell'anno</a></h2>

                    </div><!-- /article-content -->

                </div><!-- /article-wrapper -->

            </article>

        </div><!-- /box-article-small -->


<?php

    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

