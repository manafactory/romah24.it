<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_cerca_calendario extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_cerca_calendario',
            __('Cerca Contenuto per data (calendario)', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Cerca Contenuto per data (calendario)', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {
        ?>
        <div class="section-title section-title-red">

            <h3>Cerca <?php echo get_queried_object()->name;?></h3>

        </div><!-- /section-title -->
        <div class="mini-clndr" id="mini-clndr"></div>

              <script type="text/template" id="calendar-template">
                <div class="clndr-controls clearfix">
                  <div class="clndr-previous-button clearfix">&laquo;</div>
                  <div class="month clearfix"><%= month %> <%= year %></div>
                  <div class="clndr-next-button clearfix">&raquo;</div>
                </div>

                <div class="days-container clearfix">
                  <div class="days clearfix">
                    <div class="headers clearfix">
                      <% _.each(daysOfTheWeek, function(day) { %>
            <div class="day-header clearfix"><%= day %></div>
                      <% }); %>
                    </div>

                    <% _.each(days, function(day) { %>
            <div class="<%= day.classes %>"><a href="<?php echo get_post_type_archive_link(get_queried_object()->name);?>?filtra_news=1&data_1=<%= moment(day.date).format('YYYY-MM-DD') %> "><%= day.day %></a></div>
                    <% }); %>

                  </div>
                </div>
              </script>
     <?php

    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

