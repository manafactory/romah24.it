<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_lista_topics extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_lista_topics',
            __('Lista Topics', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Lista Topics', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {


        $topics_sidebar=topics_ultime_20_news();
        if ($topics_sidebar):
            ?>

            <div class="section-title section-title-red">

                <h3>Topics</h3>

            </div><!-- /section-title -->

            <div class="box box-topic">
                <?php foreach($topics_sidebar as $key => $topic):?>
                    <?php $topic_term=get_term_by('term_id', $key, 'topics');?>
                    <a href="<?php echo get_term_link($topic_term->term_id);?>"><?php echo $topic_term->name;?></a>
                <?php endforeach;?>


            </div><!-- /box-topic -->
        <?php endif;



    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

