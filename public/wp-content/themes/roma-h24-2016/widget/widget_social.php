<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_social extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_social',
            __('Social Followers', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Social Followers', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        ?>

        <div class="section-title section-title-dark section_widget_social">

            <h3><?php bloginfo('name');?> Social</h3>

        </div><!-- /section-title -->

        <div class="box box-social">

            <div class="row">

                <div class="col-md-6"><!-- col-md-6 prec. col-md-4: tolto box twitter.-->

                    <div class="box-social-content">

                        <img src="<?php bloginfo('template_url');?>/assets/icons/loghi/mini-facebook.png">

                        <p><strong><?php echo fbLikeCount();?></strong></p>

                        <small>Fans</small>

                    </div><!-- /box-social-content -->

                </div><!-- /col-md-4 -->

<!--                <div class="col-md-4">-->
<!---->
<!--                    <div class="box-social-content">-->
<!---->
<!--                        <img src="--><?php //echo get_bloginfo('template_url');?><!--/assets/icons/social-twitter.svg">-->
<!---->
<!--                        <p><strong>--><?php //echo twitter_followers();?><!--</strong></p>-->
<!---->
<!--                        <small>Followers</small>-->
<!---->
<!--                    </div>-->
<!---->
<!--                </div>-->

                <div class="col-md-6"><!-- col-md-6 prec. col-md-4: tolto box twitter.-->

                    <div class="box-social-content">

                        <img src="<?php echo get_bloginfo('template_url');?>/assets/icons/social-instagram.svg">

                        <p><strong><?php echo insta_follower();?></strong></p>

                        <small>Followers</small>

                    </div><!-- /box-social-content -->

                </div><!-- /col-md-4 -->

            </div><!-- /row -->

        </div><!-- /box-social -->
        <?php


    }

// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


}