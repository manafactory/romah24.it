<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_registrati extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_registrati',
            __('Registrati a Roma h24', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Registrati al sito', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        ?>

        <div class="panel panel-xs panel-red margin-bottom-20 registrati_h24">

            <h3>Registrati!</h3>

            <p>10 buone ragioni per entrare nella community di romah24 trieste-salario</p>

            <form>
                <input class="form-control" type="text" value="" name="" placeholder="email..." id="form-search-input" title="email...">
                <input type="submit" value="Invia">
            </form>

        </div><!-- /panel -->



<?php

    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

