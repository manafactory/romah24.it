<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_ultime_news extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_ultime_news',
            __('Ultime News', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Ultime News', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {
       $args = array(
            'posts_per_page'   => 4,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'news',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'author_name'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true
        );
$ultime_news_array = get_posts( $args );

        if ($ultime_news_array):?>
            <div class="section-title section-title-red">

                <h3>Ultime News</h3>

            </div><!-- /section-title -->
            <?php foreach($ultime_news_array as $ultima):?>

                <div class="box box-article-small">

                    <article>

                        <div class="article-wrapper">

                            <div class="article-content">

                                <h2>
                                    <?php echo get_post_time('G:i', false, $ultima->ID);?> -
                                    <a class="ultime_news_sidebar" href="<?php echo get_the_permalink($ultima->ID)?>">
                                        <?php echo get_the_title($ultima->ID)?>
                                    </a>
                                </h2>
                                <p><?php echo show_date(get_post_time('d/m/Y', false, $ultima->ID));?></p>

                            </div><!-- /article-content -->

                        </div><!-- /article-wrapper -->

                    </article>

                </div><!-- /box-article-small -->
            <?php endforeach;?>

        <?php endif;


    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

