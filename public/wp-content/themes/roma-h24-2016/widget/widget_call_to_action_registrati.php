<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_call_to_action_registrati extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_call_to_action_registrati',
            __('Call to action Contributi utenti', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Call to action Contributi utenti', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {
        echo '<div style="margin-top: 20px">';
        get_template_part('template-parts/content', 'call_to_action');
        echo '</div>';
    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

