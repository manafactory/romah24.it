<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_community_book extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_community_book',
            __('Community Book', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Community Book', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {



        $community_book_1=get_field('community_book_1','option');
        $community_book_2=get_field('community_book_2','option');

        $community_book_manuali[]=$community_book_1->ID;
        $community_book_manuali[]=$community_book_2->ID;


        if (!$community_book_1 && !$community_book_2):

            $community_book_count=0;
        else:
            if ($community_book_2 && $community_book_1):
                $community_book_count=2;
            else:
                $community_book_count=1;
            endif;
        endif;
        if ($community_book_count!=2):

            $args_cb = array(
                'posts_per_page'   => 2-$community_book_count,
                'offset'           => 0,
                'category'         => '',
                'category_name'    => '',
                'orderby'          => 'date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => $community_book_manuali,
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => 'community-book',
                'post_mime_type'   => '',
                'post_parent'      => '',
                'author'	   => '',
                'author_name'	   => '',
                'post_status'      => 'publish',
                'suppress_filters' => true,

            );

            $community_book = get_posts( $args_cb );

        endif;


        if ($community_book_2 || $community_book_1 || $community_book):
        ?>
            <div class="box box-book-container">

                <div class="section-title section-title-blue-light section_community_book">

                    <a href="<?php echo get_post_type_archive_link('community-book');?>" title="Community book"><h3>Community Book</h3></a>

                </div><!-- /section-title -->

                <?php if ($community_book_1): ?>
                    <div class="box-book">

                        <div class="box-book-thumb">

                            <a href="<?php echo get_permalink($community_book_1->ID);?>"><?php echo get_the_post_thumbnail( $community_book_1->ID, 'community_book_sidebar' ); ?></a>

                        </div><!-- /box-book-thumb -->

                        <div class="box-book-content">

                            <h4><a href="<?php echo get_permalink($community_book_1->ID);?>"><?php echo $community_book_1->post_title;?></a></h4>

                        </div><!-- /box-book-content -->

                    </div><!-- /box-book -->

                <?php endif;?>
                <?php if ($community_book_2): ?>
                    <div class="box-book">

                        <div class="box-book-thumb">

                            <a href="<?php echo get_permalink($community_book_2->ID);?>"><?php echo get_the_post_thumbnail( $community_book_2->ID, 'community_book_sidebar' ); ?></a>

                        </div><!-- /box-book-thumb -->

                        <div class="box-book-content">

                            <h4><a href="<?php echo get_permalink($community_book_2->ID);?>"><?php echo $community_book_2->post_title;?></a></h4>

                        </div><!-- /box-book-content -->

                    </div><!-- /box-book -->

                <?php endif;?>

                <?php if ($community_book_count!=2 && $community_book):?>
                    <?php foreach($community_book as $cb):?>
                    <div class="box-book">

                        <div class="box-book-thumb">

                            <a href="<?php echo get_permalink($cb->ID);?>"><?php echo get_the_post_thumbnail( $cb->ID, 'community_book_sidebar' ); ?></a>

                        </div><!-- /box-book-thumb -->

                        <div class="box-book-content">

                            <h4><a href="<?php echo get_permalink($cb->ID);?>"><?php echo $cb->post_title;?></a></h4>

                        </div><!-- /box-book-content -->

                    </div><!-- /box-book -->
                        <?php endforeach;?>
                <?php endif;?>

            </div><!-- /box-book-container -->

            <?php
            endif;



    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

