<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_ieri extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_ieri',
            __('Ieri nel Quartiere', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Ieri nel quartiere', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {



        if (have_rows('ieri_nel_quartiere','option')):?>
            <?php $titolo_sezione_ieri=get_field('ieri_nel_quartiere_titolo','option');?>
            <?php if ($titolo_sezione_ieri):?>
            <div class="section-title section-title-purple section_ieri">
                <h3><?php echo $titolo_sezione_ieri;?></h3>
            </div><!-- /section-title -->
            <?php endif;?>
            <div class="list-icons">
                <ul>
                <?php while(have_rows('ieri_nel_quartiere','option')):the_row();?>
                    <li>
                        <div class="list-icons-title left">

                            <p class="ieri_no_icons"><?php the_sub_field('label')?></p>
                            </div>
                        <p class="right"><?php the_sub_field('valore')?></p>
                    </li>

                <?php endwhile;?>
                </ul>
            </div>
        <?php

        endif;

    }

// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


}