<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_ultimi_video extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_ultimi_video',
            __('Ultimi Video', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Ultimi video', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {


        $args = array(
            'posts_per_page'   => 7,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'multimedia',
            'post__not_in'      => array(get_the_id()),
            'tax_query' => array(
                array(
                    'taxonomy' => 'categoria_multimedia',
                    'field' => 'term_id',
                    'terms' => MM_CAT_VIDEO_ID,
                )
            ),
            'post_parent'      => '',
            'author'	   => '',
            'author_name'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true
        );
        $ultimi_video_array = get_posts( $args );
        if ($ultimi_video_array):
            foreach($ultimi_video_array as $video):
        ?>
                <div class="box box-article-simple box-article-simple-aside">

                    <div class="box-article-simple-thumb thumb">
                        <a href="<?php echo get_permalink($video->ID);?>">
                            <?php echo '<span class="icon icon-play" aria-hidden="true"></span>';?>
                            <?php if (has_post_thumbnail($video->ID)):?>
                                <?php echo get_the_post_thumbnail( $video->ID, 'defunti_sidebar' ); ?>
                            <?php else:?>
                                <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/placeholder-350x210.jpg">
                            <?php endif;?>


                            </a>

                    </div><!-- /box-article-simple-thumb -->

                    <div class="box-article-simple-content">

                        <h2><a href="<?php echo get_permalink($video->ID);?>"><?php echo $video->post_title;?></a></h2>

                        <a class="link" href="<?php echo get_permalink($video->ID);?>"><?php echo $video->post_excerpt;?></a>

                    </div><!-- /box-article-simple-content -->

                </div><!-- /box-article-simple -->




        <?php endforeach;?>
            <div class="section-title section-title-blue mostra_altro_sidebar">

                <a href="<?php echo get_term_link('video', 'categoria_multimedia')?>"><h3 class="section-title-icon mostra_altro_sidebar">Mostra Altro</h3></a>

            </div><!-- /section-title -->
            <?php endif;


    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

