<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_cerca_defunto extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_cerca_defunto',
            __('Cerca una persona cara', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Cerca Una persona cara', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        ?>

        <div class="panel panel-xs panel-red margin-bottom-20 box_ricerca_persona_cara">

            <h3>Cerca una persona cara</h3>

            <p>Inserisci il nome della persona.</p>

            <form method="get" action="<?php bloginfo('url');?>/defunti/">
                <?php wp_nonce_field( 'submit', 'cerca_defunto' ); ?>
                <input class="form-control" type="text" value="" name="nome_persona" id="form-search-input" title="Inserisci il nome della persona">
                <input type="submit" value="Cerca">
            </form>

        </div><!-- /panel -->



<?php

    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

