<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_defunti extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_defunti',
            __('Ci Hanno Lasciato', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Ci Hanno Lasciato', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {


        $ci_hanno_lasciato_1=get_field('ci_hanno_lasciato_1','option');


        if (!$ci_hanno_lasciato_1):

            $ci_hanno_lasciato_1=get_random_defunto();

        endif;

        if ($ci_hanno_lasciato_1):
        ?>
              <div class="section-title section-title-blue section_defunti">

                  <a href="<?php echo get_post_type_archive_link('defunti');?>" title="Ci hanno lasciato">
                      <h3 class="section-title-icon">
                          <span class="icon icon-angel" aria-hidden="true"></span>
                          Ci ha lasciato
                      </h3>
                  </a>
              </div><!-- /section-title -->


                <?php if ($ci_hanno_lasciato_1): ?>
                    <div class="box box-article-simple box-article-simple-aside">

                        <div class="box-article-simple-thumb">

                            <a href="<?php echo get_permalink($ci_hanno_lasciato_1->ID);?>"><?php echo get_the_post_thumbnail( $ci_hanno_lasciato_1->ID, 'defunti_sidebar' ); ?></a>

                        </div><!-- /box-article-simple-thumb -->

                        <div class="box-article-simple-content">

                            <h2><a href="<?php echo get_permalink($ci_hanno_lasciato_1->ID);?>"><?php echo $ci_hanno_lasciato_1->post_title;?></a></h2>

                            <a class="link section_defunti" href="<?php echo get_permalink($ci_hanno_lasciato_1->ID);?>">Il tuo ricordo</a>

                        </div><!-- /box-article-simple-content -->

                    </div><!-- /box-article-simple -->

                <?php endif;?>





            <?php
            endif;



    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

