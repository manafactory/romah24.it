<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_info_evento extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_info_evento',
            __('Info evento', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Info Evento', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        $luogo_evento=get_field('luogo', get_the_ID());
        $data_evento=get_field('data', get_the_ID());
        $data_evento_finale=get_field('data_e_orario_finali', get_the_ID());
        $date_escluse=get_field('date_escluse', get_the_ID());
        $mail_evento=get_field('evento_mail_contatto', get_the_ID());
        $telefono_evento=get_field('evento_telefono_contatto', get_the_ID());
        $sito_web_evento=get_field('evento_sito_web', get_the_ID());




        ?>

        <div class="container_dett">

        <?php if ($luogo_evento):
            ?>

            <div class="box box-article-simple box-article-simple-aside dett_evento">

                <div class="box-article-simple-thumb thumb dett_evento_icona_cont">
                    <span class="icon icon-luogo dett_evento" aria-hidden="true"></span>


                </div><!-- /box-article-simple-thumb -->

                <div class="box-article-simple-content dett_evento_text_cont">
                    <?php $url_address=geocode($luogo_evento); ?>
                    <p>
                        <?php if (wp_is_mobile()):?>
                            <a href="geo:0,0?q=<?php echo $url_address[2];?>">
                                <?php echo $luogo_evento;?>
                            </a>
                        <?php else:?>
                            <a href="https://www.google.it/maps/place/<?php echo $url_address[2];?>" target="_blank">
                                <?php echo $luogo_evento;?>
                            </a>
                        <?php endif;?>
                    </p>

                </div><!-- /box-article-simple-content -->

            </div><!-- /box-article-simple -->


        <?php
        endif;
        if ($data_evento):
            $data_array=explode(" ",$data_evento);

            $data_evento_to_show="";

            if ($data_array):
                $data_to_show=$data_array[0];
                $clock_to_show=$data_array[1];
                $ore_min_sec=explode(":",$clock_to_show);
                $clock_to_show= $ore_min_sec[0].':'.$ore_min_sec[1];

                $data_evento_to_show=show_date($data_to_show).' - '.$clock_to_show;
            else:

                $data_evento_to_show=$data_evento;

            endif;

            $data_finale_array=explode(" ",$data_evento_finale);

            $data_finale_evento_to_show="";

            if ($data_finale_array):
                $data_to_show=$data_finale_array[0];
                $clock_to_show=$data_finale_array[1];
                $ore_min_sec=explode(":",$clock_to_show);
                $clock_to_show= $ore_min_sec[0].':'.$ore_min_sec[1];

                $data_finale_evento_to_show=show_date($data_to_show).' - '.$clock_to_show;
            else:

                $data_finale_evento_to_show=$data_evento;

            endif;

            ?>

            <div class="box box-article-simple box-article-simple-aside dett_evento">

                <div class="box-article-simple-thumb thumb dett_evento_icona_cont">
                    <span class="icon icon-data dett_evento" aria-hidden="true"></span>


                </div><!-- /box-article-simple-thumb -->

                <div class="box-article-simple-content dett_evento_text_cont" style="padding-top: 5px;">

                    <p class="date_escluse" style="color: #000;">Dal <?php echo $data_evento_to_show;?></p>
                    <?php if ($data_finale_evento_to_show):?>
                        <p class="date_escluse" style="color: #000;">al <?php echo $data_finale_evento_to_show;?></p>
                    <?php endif;?>
                </div><!-- /box-article-simple-content -->

            </div><!-- /box-article-simple -->


        <?php
        endif;

        $date_escluse_html='';
        if ($date_escluse):
            $date_escluse_html=$date_escluse_html.'<p class="date_escluse_titolo">Date escluse:</p> ';

            foreach($date_escluse as $data_esclusa):

                $date_escluse_html=$date_escluse_html.'<p class="date_escluse">'.show_date($data_esclusa['data_esclusa']).'</p>';
            endforeach;
            ?>

            <div class="box box-article-simple box-article-simple-aside dett_evento date_escluse_container">

                <div class="box-article-simple-thumb thumb dett_evento_icona_cont">
                    <span class="icon icon-data dett_evento" title="Date escluse" style="color: #000 !important; border: 1px solid #000 !important;" aria-hidden="true"></span>
                </div><!-- /box-article-simple-thumb -->

                <div class="box-article-simple-content dett_evento_text_cont">

                    <?php echo $date_escluse_html;?>

                </div><!-- /box-article-simple-content -->

            </div><!-- /box-article-simple -->


        <?php
        endif;
        if ($telefono_evento):
            ?>

            <div class="box box-article-simple box-article-simple-aside dett_evento">

                <div class="box-article-simple-thumb thumb dett_evento_icona_cont">
                    <span class="icon icon-telefono dett_evento" aria-hidden="true"></span>


                </div><!-- /box-article-simple-thumb -->

                <div class="box-article-simple-content dett_evento_text_cont">
                    <p>
                        <?php if (wp_is_mobile()):?>
                            <a href="tel:+<?php echo $telefono_evento;?>">
                                <?php echo $telefono_evento;?>
                            </a>
                        <?php else:?>
                            <?php echo $telefono_evento;?>
                        <?php endif;?>
                    </p>

                </div><!-- /box-article-simple-content -->

            </div><!-- /box-article-simple -->


        <?php
        endif;
        if ($mail_evento):
            ?>

            <div class="box box-article-simple box-article-simple-aside dett_evento">

                <div class="box-article-simple-thumb thumb dett_evento_icona_cont">
                    <span class="icon icon-mail dett_evento" aria-hidden="true"></span>


                </div><!-- /box-article-simple-thumb -->

                <div class="box-article-simple-content dett_evento_text_cont">

                    <p>
                        <a href="mailto:<?php echo $mail_evento;?>">
                            <?php echo $mail_evento;?>
                        </a>
                    </p>

                </div><!-- /box-article-simple-content -->

            </div><!-- /box-article-simple -->


        <?php
        endif;

        if ($sito_web_evento):
            ?>

            <div class="box box-article-simple box-article-simple-aside dett_evento">

                <div class="box-article-simple-thumb thumb dett_evento_icona_cont">
                    <span class="icon icon-sito dett_evento" aria-hidden="true"></span>


                </div><!-- /box-article-simple-thumb -->

                <div class="box-article-simple-content dett_evento_text_cont">
                    <p><a href="<?php echo $sito_web_evento;?>"><?php echo $sito_web_evento;?></a></p>

                </div><!-- /box-article-simple-content -->

            </div><!-- /box-article-simple -->


        <?php
        endif;

        ?>

        </div>
        <?php



    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

