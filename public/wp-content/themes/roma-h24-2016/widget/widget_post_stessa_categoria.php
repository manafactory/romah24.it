<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_post_stessa_categoria extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_post_stessa_categoria',
            __('Post Categoria', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box che recupera i post della stessa categoria', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        $categoria='';

        if (get_post_type(get_the_ID())=='news'):
            $category_to_check='categoria';
        else:
            $category_to_check='categoria_'.get_post_type(get_the_ID());
        endif;

        $post_terms = wp_get_post_terms( get_the_ID(), $category_to_check);

        if ($post_terms):

                $term_id_to_check=$post_terms[0]->term_id;

                $args = array(
                    'posts_per_page'   => 5,
                    'offset'           => 0,
                    'category'         => '',
                    'category_name'    => '',
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'include'          => '',
                    'exclude'          => '',
                    'meta_key'         => '',
                    'meta_value'       => '',
                    'post_type'        => get_post_type(get_the_ID()),
                    'post__not_in'      => array(get_the_id()),
                    'tax_query' => array(
                        array(
                            'taxonomy' => $category_to_check,
                            'field' => 'term_id',
                            'terms' => $term_id_to_check,
                        )
                    ),
                    'post_parent'      => '',
                    'author'	   => '',
                    'author_name'	   => '',
                    'post_status'      => 'publish',
                    'suppress_filters' => true
                );
                $post_categoria_array = get_posts( $args );

                    if ($post_categoria_array):?>
                        <div class="section-title section-title-red">

                            <h3>Correlati</h3>

                        </div><!-- /section-title -->

                        <?php foreach ($post_categoria_array as $single_post):?>
                            <div class="box box-article-small">

                                <article>

                                    <div class="article-wrapper">

                                        <div class="article-content">

                                            <h2><a href="<?php echo get_the_permalink($single_post->ID)?>">> <?php echo get_the_title($single_post->ID)?></a></h2>

                                        </div><!-- /article-content -->

                                    </div><!-- /article-wrapper -->

                                </article>

                            </div><!-- /box-article-small -->


                        <?php endforeach;
                    endif;
            ?>




<?php
endif;
    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

