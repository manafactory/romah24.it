<?php


require_once('widget_ieri.php');
require_once('widget_community_book.php');
require_once('widget_social.php');
require_once('widget_banner_half_page.php');
require_once('widget_defunti.php');
require_once('widget_registrati.php');
require_once('widget_necrologi_recenti.php');
require_once('widget_cerca_defunto.php');
require_once('widget_ultimi_video.php');
require_once('widget_cerca_calendario.php');
require_once('widget_ultime_news.php');
require_once('widget_post_stessa_categoria.php');
require_once('widget_info_evento.php');
require_once('widget_lista_topics.php');
require_once('widget_call_to_action_registrati.php');

function wpb_load_widget() {
    register_widget( 'widget_ieri' );
    register_widget( 'widget_community_book' );
    register_widget( 'widget_social' );
    register_widget( 'widget_banner_half_page' );
    register_widget( 'widget_defunti' );
    register_widget( 'widget_cerca_defunto' );
    register_widget( 'widget_registrati' );
    register_widget( 'widget_necrologi_recenti' );
    register_widget( 'widget_ultimi_video' );
    register_widget( 'widget_cerca_calendario' );
    register_widget( 'widget_ultime_news' );
    register_widget( 'widget_post_stessa_categoria' );
    register_widget( 'widget_info_evento' );
    register_widget( 'widget_lista_topics' );
    register_widget( 'widget_call_to_action_registrati' );

}

add_action( 'widgets_init', 'wpb_load_widget' );
