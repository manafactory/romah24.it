<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Roma_H24_2016
 */

get_header(); ?>


      <div class="top-title">
        <?php
            if (isset($_GET['data_1']) || $_GET['filtra_news']==1):
                $data_filtro_to_show=filtro_data_da_mostrare($_GET['data_1']);
            endif;
        ?>
        <h1>News<?php if (($data_filtro_to_show)): echo $data_filtro_to_show; endif;?></h1>

          <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

      </div>


      <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

      </div><!-- /adv -->


      <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

          <div class="col-md-8">
<?php if (have_posts()):?>
            <?php global $data_to_check; $data_to_check='';?>

            <?php while ( have_posts() ) : the_post();?>

                <?php get_template_part('template-parts/content', 'box_liste_no_foto');?>


            <?php endwhile;?>

<?php else:?>
    <?php get_template_part('template-parts/content', 'nessun_contenuto');?>

<?php endif;
remove_action('pre_get_posts','filtra_post_per_giorno');
?>
          </div><!-- /col-md-8 -->

<?php get_sidebar();?>
        </div><!-- /row -->
        <div class="row row-margin-0 margin-bottom-20">
            <div class="col-md-8">
                <div class="pagination_container">
                    <?php my_pagination();?>
                </div>

             </div>
        </div>
      </main>

    </div><!-- /container -->
<?php
get_footer();
