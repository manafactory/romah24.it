<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Roma_H24_2016
 */

get_header(); ?>

<?php
while ( have_posts() ) : the_post();
    ?>
    <div class="top-title">

        <p class="h1"><?php echo get_the_title();?></p>

        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>

    </div>


    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->


    <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

            <div class="col-md-8">

                <?php get_template_part('template-parts/content', 'sharing');?>

                <div class="post-content red_link">

                    <h1><?php echo get_the_title();?></h1>
                    <?php if (has_post_thumbnail()):?>
                        <figure>
                            <?php $thumb_description=get_thumb_descritpion(get_the_ID());?>
                            <?php $default_attr = array('class'	=> "img-responsive",'alt'	=> $thumb_description);?>
                            <?php the_post_thumbnail('', $default_attr);?>
                            <figcaption><?php echo $thumb_description;?></figcaption>
                        </figure>
                    <?php endif;?>
                    <?php the_content();?>

                </div>




            </div><!-- /col-md-8 -->




        </div><!-- /row -->

    </main>

<?php endwhile; // End of the loop.
?>
    </div><!-- /container -->
<?php
get_footer();
