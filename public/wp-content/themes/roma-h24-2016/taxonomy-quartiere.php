<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 20/01/17
 * Time: 12.24
 */

get_header(); ?>

<?php if (have_posts()):
    $queried_obj=get_queried_object();
    if (isset($_GET['post_type'])):
        $post_type=$_GET['post_type'];
    else:
        $post_type='news';
    endif;
    ?>

    <div class="top-title">

        <h1><?php echo $queried_obj->name;?></h1>
        <div class="breadcrumbs"><?php roma_h24_breadcrumb();?></div>
    </div>


    <div class="adv adv-leaderboard">

        <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/top-leaderboard.jpg">

    </div><!-- /adv -->


    <main class="main_container" id="main">

        <div class="row row-margin-0 margin-bottom-20">

            <div class="col-md-8">
                <?php $data_to_check='';?>

                <?php while ( have_posts() ) : the_post();?>
                    <?php if (get_post_type(get_the_ID())=='news'):?>

                        <?php get_template_part('template-parts/content', 'box_liste_no_foto');?>
                    <?php else:?>
                        <?php get_template_part('template-parts/content', 'box_liste_foto');?>
                    <?php endif;?>
                <?php endwhile;?>


            </div><!-- /col-md-8 -->

            <?php get_sidebar('news');?>
        </div><!-- /row -->
        <div class="row row-margin-0 margin-bottom-20">
            <div class="col-md-8">
                <div class="pagination_container">
                    <?php my_pagination();?>
                </div>

            </div>
        </div>
    </main>
<?php endif;?>

    </div><!-- /container -->
<?php
get_footer();
