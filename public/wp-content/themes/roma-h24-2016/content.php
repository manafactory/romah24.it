
<?php get_template_part('template-parts/content', 'sharing');?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php

$data_news=get_post_time('d:m:Y', false);
$data_news_to_show=get_post_time('d/m/Y', false);

$categoria_news=wp_get_post_terms(get_the_ID(),'categoria');
$quartiere=wp_get_post_terms(get_the_ID(),'quartiere');

?>
    <h3 class="news_dettaglio_data_e_ora">
        <?php $gmt_timestamp = get_post_time('G:i', false);?>
        <?php echo show_date($data_news_to_show);?> - <?php echo $gmt_timestamp;?>
        <?php if ($quartiere):
            echo '. <a href="'.get_term_link($quartiere[0]->term_id).'" title="'.$quartiere[0]->name.'">';
            echo $quartiere[0]->name;
            echo '</a>';
        endif;?>

        <?php if ($categoria_news):
            echo '. <a href="'.get_term_link($categoria_news[0]->term_id).'" title="'.$categoria_news[0]->name.'">';
            echo $categoria_news[0]->name;
            echo '</a>';
        endif;?>
    </h3>
    <h2 class="news-title">
        <?php echo get_the_title();?>
    </h2>
    <h5 class="h5_post_author">
        di <?php echo autore();?>
    </h5>
    <?php if (has_post_thumbnail()):?>
        <figure>
            <?php $thumb_description=get_thumb_descritpion(get_the_ID());?>
            <?php $default_attr = array('class'	=> "img-responsive",'alt'	=> $thumb_description);?>
            <?php the_post_thumbnail('', $default_attr);?>
            <figcaption><?php echo $thumb_description;?></figcaption>
        </figure>
    <?php endif;?>
    <?php the_content();?>
    <?php $topics=wp_get_post_terms(get_the_ID(),'topics');?>
    <?php if ($topics):?>
        <div class="trend single_news">
            <span class="icon icon-arrow-right" aria-hidden="true"></span>

            <div class="trend-list">
                <?php foreach ($topics as $topic):?>
                    <a class="trend_single_news" href="<?php echo get_term_link($topic->term_id);?>"><?php echo $topic->name;?></a>

                <?php endforeach;?>

            </div><!-- /trend-list -->
        </div><!-- /trend -->
    <?php endif;?>

    <div class="correlati_post">
        <?php
        $speciali_correlati=get_field('speciali_correlati');
        $eventi_correlati=get_field('eventi_correlati');
        $community_book_correlati=get_field('community_book_correlati');
        $multimedia_correlati=get_field('multimedia_correlati');
        if ($speciali_correlati || $eventi_correlati || $community_book_correlati || $multimedia_correlati):
        ?>
            <div class="section-title section-title-red">

                <h3>Leggi anche</h3>

            </div><!-- /section-title -->

        <?php
        endif;

    if ($speciali_correlati):?>

        <?php foreach ($speciali_correlati as $speciale):?>
            <div class="box box-article-small">

                <article>

                    <div class="article-wrapper">

                        <div class="article-content">

                            <h2><a href="<?php echo get_the_permalink($speciale->ID)?>">> <?php echo get_the_title($speciale->ID)?></a></h2>

                        </div><!-- /article-content -->

                    </div><!-- /article-wrapper -->

                </article>

            </div><!-- /box-article-small -->


        <?php endforeach;
    endif;?>
    <?php

    if ($eventi_correlati):?>

        <?php foreach ($eventi_correlati as $evento):?>
            <div class="box box-article-small">

                <article>

                    <div class="article-wrapper">

                        <div class="article-content">

                            <h2><a href="<?php echo get_the_permalink($evento->ID)?>">> <?php echo get_the_title($evento->ID)?></a></h2>

                        </div><!-- /article-content -->

                    </div><!-- /article-wrapper -->

                </article>

            </div><!-- /box-article-small -->


        <?php endforeach;
    endif;?>
    <?php

    if ($community_book_correlati):?>

        <?php foreach ($community_book_correlati as $c_book):?>
            <div class="box box-article-small">

                <article>

                    <div class="article-wrapper">

                        <div class="article-content">

                            <h2><a href="<?php echo get_the_permalink($c_book->ID)?>">> <?php echo get_the_title($c_book->ID)?></a></h2>

                        </div><!-- /article-content -->

                    </div><!-- /article-wrapper -->

                </article>

            </div><!-- /box-article-small -->


        <?php endforeach;
    endif;?>
    <?php

    if ($multimedia_correlati):?>

        <?php foreach ($multimedia_correlati as $multimedia):?>
            <div class="box box-article-small">

                <article>

                    <div class="article-wrapper">

                        <div class="article-content">

                            <h2><a href="<?php echo get_the_permalink($multimedia->ID)?>">> <?php echo get_the_title($multimedia->ID)?></a></h2>

                        </div><!-- /article-content -->

                    </div><!-- /article-wrapper -->

                </article>

            </div><!-- /box-article-small -->


        <?php endforeach;
    endif;?>
    </div>


</article>


