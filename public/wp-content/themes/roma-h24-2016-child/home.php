<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 29/12/16
 * Time: 12.09
 */


get_header();
?>

<?php
$topics_in_evidenza= get_field('topics_in_evidenza', 'option');
$topics_in_evidenza_counter=count($topics_in_evidenza);
$topics_mostrati=array();
if (!$topics_in_evidenza || $topics_in_evidenza_counter<6):
    if ($topics_in_evidenza):
        foreach ($topics_in_evidenza as $topics):
            $topics_mostrati[]=$topics->term_id;
        endforeach;
    endif;
    $topics_in_evidenza_to_6 = topics_automatici($topics_mostrati);
endif;
?>


<div class="trend">
<?php if ($topics_in_evidenza || $topics_in_evidenza_to_6):?>
    <span class="icon icon-arrow-right" aria-hidden="true"></span>

    <div class="trend-list">
        <?php if ($topics_in_evidenza):?>
            <?php foreach ($topics_in_evidenza as $topics):?>
                <a href="<?php echo get_term_link($topics->term_id);?>"><?php echo $topics->name;?></a>

            <?php endforeach;?>
        <?php endif;?>
        <?php if ($topics_in_evidenza_to_6):?>
            <?php foreach ($topics_in_evidenza_to_6 as $topics):?>
                    <a href="<?php echo get_term_link($topics->term_id);?>"><?php echo $topics->name;?></a>
            <?php endforeach;?>
        <?php endif;?>

    </div><!-- /trend-list -->
<?php endif;?>

</div><!-- /trend -->
<?php $leaderboard=get_field('top_leaderboard', 'option');?>

<?php if ($leaderboard):?>
<div class="adv adv-leaderboard">

    <?php if ($leaderboard):?>
        <img src="<?php echo $leaderboard['sizes']['top_leaderboard'];?>">
    <?php endif;?>

</div><!-- /adv -->
<?php endif;?>

<main class="main_container" id="main">
<?php
$news_evi_full_boxed=get_field('news_full_boxed','option');

if ($news_evi_full_boxed=='full'):
    $content_to_get='news_full';
else:
    $content_to_get='news_boxed';
endif;

?>
<?php  get_template_part('template-parts/content', $content_to_get);?>

    <div class="row row-margin-0 margin-bottom-20">

        <div class="col-md-8">

            <div class="box box-list home-news-list">

                <div class="section-title section-title-red">

                    <h3>Ultim'ora</h3>

                </div><!-- /section-title -->

                <?php
                    $args_ultima_ora=array(
                                            'posts_per_page'   => 25,
                                            'offset'           => 0,
                                            'category'         => '',
                                            'category_name'    => '',
                                            'orderby'          => 'date',
                                            'order'            => 'DESC',
                                            'include'          => '',
                                            'exclude'          => '',
                                            'meta_key'         => '',
                                            'meta_value'       => '',
                                            'post_type'        => 'news',
                                            'post_mime_type'   => '',
                                            'post_parent'      => '',
                                            'author'	   => '',
                                            'author_name'	   => '',
                                            'post_status'      => 'publish',
                                            'suppress_filters' => false
                                            );
                    $news_ultima_ora = get_posts( $args_ultima_ora );
                ?>


                <?php $news_counter=0; if ($news_ultima_ora): ?>

                <ul>
                    <?php foreach ($news_ultima_ora as $ultima): $news_counter++;

                        $gmt_timestamp = get_post_time('G:i', false, $ultima->ID);
                        $categoria_news=wp_get_post_terms($ultima->ID,'categoria');
                    ?>

                    <li>
                        <div class="time"><?php echo $gmt_timestamp;?></div>
                        <div class="article">
                        <?php $logo_news=get_field('logo_news', $ultima->ID);?>
                        <?php if ($logo_news):?>

                            <div class="article-content article-thumb-content">
                                <div class="article-thumb">
                                    <img src="<?php echo $logo_news['sizes']['logo_news'];?>">
                                </div>
                                <article>
                                    <a <?php if ($news_counter==1): echo 'style="color:#D0021B;"'; endif;?> href="<?php echo get_permalink($ultima->ID);?>">
                                        <?php if ($categoria_news):
                                            echo "<b>".strtoupper($categoria_news[0]->name)."</b>";
                                            echo '. ';

                                        endif;?>
                                        <?php echo $ultima->post_title;?>
                                    </a>
                                </article>
                            </div>
                        <?php else:?>
                            <div class="article-content">
                                <article>
                                    <a <?php if ($news_counter==1): echo 'style="color:#D0021B;"'; endif;?> href="<?php echo get_permalink($ultima->ID);?>">
                                        <?php if ($categoria_news):
                                            echo "<b>".strtoupper($categoria_news[0]->name)."</b>";
                                            echo '. ';

                                        endif;?>
                                        <?php echo $ultima->post_title;?></a>
                                </article>
                            </div>
                        <?php endif;?>
                        </div>
                    </li>
                    <?php endforeach;?>

                </ul>

            </div><!-- /box-list -->

            <?php endif;?>

<?php  get_template_part('template-parts/content', 'box_oggi_e_domani');?>

<?php $titolo_coupon = get_field('titolo_coupon', 'option');?>
<?php $coupon_img = get_field('immagine_coupon', 'option');?>
            <?php $coupon_link = get_field('link_coupon', 'option');?>

            <?php if ($coupon_link && $coupon_img):?>
            <div class="section-title section-title-blue-light">
                <?php if ($titolo_coupon):?>
                    <h3><?php echo $titolo_coupon;?></h3>
                <?php endif;?>
            </div><!-- /section-title -->

            <div class="box box-coupon">

                <?php if ($coupon_link):?>

                    <a href="<?php echo $coupon_link;?>">
                        <img src="<?php echo $coupon_img['sizes']['coupon_home'];?>">
                    </a>

                    <a class="btn btn-default btn-sm" href="<?php echo $coupon_link;?>">
                        Clicca qui
                    </a>
                <?php else:?>
                    <img src="<?php echo $coupon_img['sizes']['coupon_home'];?>">
                <?php endif;?>

            </div><!-- /box-coupon -->
            <?php endif;?>
        </div><!-- /col-md-8 -->


<?php get_sidebar();?>
    </div><!-- /row -->
<?php

$tv_array=get_field('video_in_evidenza','option');

if (!$tv_array):

$args_tv = array(
    'posts_per_page'   => 2,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'multimedia',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
);
//$tv_array = get_posts( $args_tv );
endif;

if ($tv_array):
?>


    <div class="section-title section-title-orange">

        <h3><?php bloginfo('name');?> TV</h3>

    </div><!-- /section-title -->
    <div class="row margin-bottom-30">
    <?php if ($tv_array[0]):?>
        <div class="col-md-8">

            <div class="box box-video">

                <a href="<?php echo get_permalink($tv_array[0]->ID);?>">
                    <?php if (is_video($tv_array[0]->ID)):?>
                        <span class="icon icon-play" aria-hidden="true"></span>
                    <?php endif;?>

                    <div class="tag"><?php bloginfo('name');?> TV</div>

                    <h2><?php echo $tv_array[0]->post_title;?></h2>

                    <div class="box-video-opacity"></div>
                    <?php
                    if (has_post_thumbnail($tv_array[0]->ID)):
                        $url_image_copertina=get_the_post_thumbnail_url($tv_array[0]->ID, 'copertina_video_tv_home');
                    else:
                        $url_image_copertina=get_bloginfo('template_url').'/assets/placeholders/placeholder-featured.jpg';
                    endif;
                    ?>

                    <div class="box-video-thumb" style="background-image: url('<?php echo $url_image_copertina;?>');"></div>

                </a>

            </div><!-- /box-video -->

        </div><!-- /col-md-8 -->
    <?php endif;?>

    <?php  //get_template_part('template-parts/content', 'borsino');?>
    </div><!-- /row -->

<?php endif;?>

<?php $args_segnalazioni = array(
    'posts_per_page'   => 8,
    'offset'           => 0,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'news',
    'post_status'      => 'publish',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'contributo_associato',
            'value' => '',
            'compare' => '!='
        ),
        array(
            'key' => '_thumbnail_id',
            'compare' => 'EXISTS'
        ),
    ),
    'suppress_filters' => true
);
$segnalazioni_utenti_array = get_posts( $args_segnalazioni );?>

<?php if ($segnalazioni_utenti_array):?>
    <div class="section-title section-title-red-darker">

        <h3>Dalla nostra community</h3>

    </div><!-- /section-title -->

    <div class="box box-five-container margin-bottom-30">
        <?php foreach ($segnalazioni_utenti_array as $segnalazione):?>
        <div class="box box-five">

            <a href="<?php echo get_permalink($segnalazione->ID);?>">
                <?php if (has_post_thumbnail( $segnalazione->ID)):?>
                    <?php echo get_the_post_thumbnail($segnalazione->ID,'community_book_home');?>
                <?php else:?>
                    <img src="<?php echo get_bloginfo('template_url');?>/assets/placeholders/placeholder-community-simple.jpg">
                <?php endif;?>
            </a>

        </div>
        <?php endforeach;?>

    </div>
<?php endif;?>
</main>

</div><!-- /container -->

<?php get_footer();?>