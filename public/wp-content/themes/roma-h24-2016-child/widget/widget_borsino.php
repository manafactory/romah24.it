<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_borsino extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_borsino',
            __('Vetrina', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Vetrina', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {
        if (have_rows('borsino_oggetti', 'option')):
    ?>

            <div class="section-title section-title-orange">

                <h3>Vetrina</h3>

            </div><!-- /section-title -->

            <?php while(have_rows('borsino_oggetti', 'option')): the_row();?>

                <div class="box box-article-simple box-article-simple-product">

                    <div class="box-article-simple-thumb">

                        <img src="<?php echo get_sub_field('immagine')['sizes']['borsino'];?>">

                    </div><!-- /box-article-simple-thumb -->

                    <div class="box-article-simple-content">

                        <h2><?php the_sub_field('titolo');?></h2>

                        <p><?php the_sub_field('descrizione');?></p>

                        <div class="product-bottom">

                            <span><?php the_sub_field('prezzo');?></span>

                            <a class="btn btn-default btn-xs buy-vetrina" href="<?php the_sub_field('link_acquisto');?>">Acquista</a>

                        </div>

                    </div><!-- /box-article-simple-content -->

                </div><!-- /box-article-simple -->


            <?php endwhile;?>
        <?php endif;?>

<?php
    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

