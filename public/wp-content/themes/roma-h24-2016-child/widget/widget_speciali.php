<?php
/**
 * Created by PhpStorm.
 * User: alessandroambrosini
 * Date: 11/04/17
 * Time: 12:13
 */


class widget_speciali extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_speciali',
            __('Speciali', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Speciali', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {



        if (have_rows('speciali_sidebar', 'option')){
            ?>
            <div class="section-title section-title-blue">

                <h3>Speciali</h3>

            </div>

            <?php
            while (have_rows('speciali_sidebar', 'option')){
                the_row();?>
                <div class="box box-article-simple box-article-simple-aside">

                    <div class="box-article-simple-thumb">

                        <a href="<?php echo get_permalink(get_sub_field('speciale')->ID);?>"><img src="<?php echo get_the_post_thumbnail_url(get_sub_field('speciale')->ID, 'defunti_sidebar')?>"></a>

                    </div><!-- /box-article-simple-thumb -->

                    <div class="box-article-simple-content">

                        <h2><a href="<?php echo get_permalink(get_sub_field('speciale')->ID);?>"><?php echo (get_sub_field('speciale')->post_title);?></a></h2>

                        <a class="link" href="<?php echo get_permalink(get_sub_field('speciale')->ID);?>">Leggi >></a>

                    </div><!-- /box-article-simple-content -->

                </div>
                <?php


                }


        }
?>


        <?php
    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
        <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

