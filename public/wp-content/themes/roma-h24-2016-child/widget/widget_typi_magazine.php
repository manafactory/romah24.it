<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 12/01/17
 * Time: 10.35
 */



// Creating the widget
class widget_typi_magazine extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_typi_magazine',
            __('Typi Magazine (RSS)', 'wpb_widget_domain'),
            array( 'description' => __( 'Gestione box Typi Magazine (Tramite Rss)', 'wpb_widget_domain' ), )
        );
    }

    public function widget( $args, $instance ) {

        $content = file_get_contents('http://www.typi.it/magazine/feed/');
        $x = new SimpleXmlElement($content);
        $c=0;


    if ($content){ ?>
        <div class="box box-book-container new_typi_mag">

        <div class="section-title section-title-blue-light new_typi_mag_title_label">

            <a href="<?php ?>" title="Community book"><h3>Typi Magazine</h3></a>

        </div><!-- /section-title -->


        <?php
        foreach($x->channel->item as $entry) {
            $c++;
            if ($c<=3){
                ?>
                <div class="box box-article-simple box-article-simple-aside">


                    <div class="box-article-simple-content new_typi_mag_content">

                        <h2><a href="<?php echo $entry->link?>" target="_blank">
                                <?php echo $entry->title?>
                                <a class="link" href="<?php echo $entry->link?>" target="_blank"> &gt;&gt;</a></a></h2>



                    </div><!-- /box-article-simple-content -->

                </div>

                <?php
            }
        }


        ?>
        </div><!-- /box-book-container -->

    <?php }
        ?>



            <?php



    }


// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }


} // Class wpb_widget ends here

