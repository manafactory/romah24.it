<?php


//require_once('widget_borsino.php');
require_once('widget_typi_magazine.php');
require_once('widget_speciali.php');
require_once('widget_borsino.php');

function wpb_load_widget_child() {
    //register_widget( 'widget_borsino' );
    register_widget( 'widget_typi_magazine' );
    register_widget( 'widget_speciali' );
    register_widget( 'widget_borsino' );

}

add_action( 'widgets_init', 'wpb_load_widget_child' );
