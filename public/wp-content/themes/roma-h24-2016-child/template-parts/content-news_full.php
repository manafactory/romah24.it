<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 18/01/17
 * Time: 11.48
 */


?>

<?php
$news_evi_titolo=get_field('news_titolo','option');
$news_evi_foto=get_field('news_foto','option');
$news_evi_sommario=get_field('news_sommario','option');
$news_evi_link=get_field('news_link','option');
$news_evi_full_boxed=get_field('news_full_boxed','option');
$news_evi_icona_foto=get_field('news_icona_foto','option');
if ($news_evi_icona_foto=='video'):
    $class_icona='icon-video_new';
elseif($news_evi_icona_foto=='img'):
    $class_icona='icon-photo';
else:
    $class_icona='no_icona';
endif;

?>
<?php if ($news_evi_link && $news_evi_titolo && $news_evi_foto):?>

    <div class="row row-margin-0">
        <div class="col-md-12">

            <div class="box box-featured full_width_news_title">


                <div class="box-featured-content box_news_black">

                    <h2><a href="<?php echo $news_evi_link; ?>"><?php echo $news_evi_titolo; ?></a></h2>

                    <div class="box-featured-abstract <?php if ($news_evi_icona_foto=='no'): echo 'no_padding_left'; endif;?>">
                        <?php if ($news_evi_icona_foto!='no'):?>
                            <span class="icon <?php echo $class_icona;?>" aria-hidden="true"></span>
                        <?php endif;?>
                        <?php echo $news_evi_sommario;?>

                    </div>

                </div><!-- /box-featured-content -->
            </div>
        </div>
    </div>
    <?php endif;?>
    <div class="row row-margin-0 margin-bottom-20">
        <?php if ($news_evi_link && $news_evi_titolo && $news_evi_foto):?>
        <div class="col-md-8">
            <div class="box box-featured">
                <div class="tag">
                    <a href="<?php echo get_post_type_archive_link('news'); ?>">News</a>
                </div><!-- /tag -->

                <?php

                if ($news_evi_foto):

                    $url_thumb=$news_evi_foto['sizes']['news_evidenza'];

                else:

                    $url_thumb=get_bloginfo('template_url').'/assets/placeholders/placeholder-featured.jpg';
                endif;

                ?>

                <div class="box-featured-thumb" style="background-image: url('<?php echo $url_thumb;?>');"></div>

            </div><!-- /box-featured -->
        </div><!-- /col-md-8 -->
        <?php endif;?>


        <div class="col-md-4">

            <?php $banner_300_250=get_field('mpu_300_250','option');?>
            <?php if ($banner_300_250):?>
                <div class="adv adv-mpu adv-mpu-home-top banner_home_300_250_container">

                    <img class="banner_home_300_250" src="<?php echo $banner_300_250['sizes']['banner_sidebar_300_250'];?>">

                </div><!-- /adv -->
            <?php endif;?>
            <?php  get_template_part('template-parts/content', 'call_to_action');?>

        </div><!-- /col-md-4 -->
    </div><!-- /row -->
