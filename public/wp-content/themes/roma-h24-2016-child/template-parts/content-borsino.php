<?php
/**
 * Created by PhpStorm.
 * User: alessandroambrosini
 * Date: 20/03/17
 * Time: 11:56
 */

if (have_rows('borsino_oggetti', 'option')):
    ?>
    <div class="col-md-4">

    <div class="section-title section-title-purple-bright">

        <h3>Borsino</h3>

    </div><!-- /section-title -->

    <?php while(have_rows('borsino_oggetti', 'option')): the_row();?>

    <div class="box box-article-simple box-article-simple-product">

        <div class="box-article-simple-thumb">

            <img src="<?php echo get_sub_field('immagine')['sizes']['borsino'];?>">

        </div><!-- /box-article-simple-thumb -->

        <div class="box-article-simple-content">

            <h2><?php the_sub_field('titolo');?></h2>

            <p><?php the_sub_field('descrizione');?></p>

            <div class="product-bottom">

                <span><?php the_sub_field('prezzo');?></span>

                <a class="btn btn-default btn-xs" href="<?php the_sub_field('link_acquisto');?>">Acquista</a>

            </div>

        </div><!-- /box-article-simple-content -->

    </div><!-- /box-article-simple -->


<?php endwhile;?>
    </div>
<?php endif;?>