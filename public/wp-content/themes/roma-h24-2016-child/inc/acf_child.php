<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 01/03/17
 * Time: 17.28
 */


function sub_page(){
    acf_add_options_sub_page(array(
        'page_title' => 'Vetrina',
        'menu_title' => 'Vetrina',
        'parent_slug' => 'theme-general-settings',
    ));

}

add_action('init', 'sub_page');


    function romah24_acf_sections()
    {
        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page(array(
                'page_title' => 'Gestione',
                'menu_title' => 'Gestione',
                'menu_slug' => 'theme-general-settings',
                'capability' => 'edit_posts',
                'redirect' => true
            ));


            acf_add_options_sub_page(array(
                'page_title' => 'News in evidenza Settings',
                'menu_title' => 'News in evidenza',
                'parent_slug' => 'theme-general-settings',
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'Video in Evidenza Settings',
                'menu_title' => 'Video in Evidenza',
                'parent_slug' => 'theme-general-settings',
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'Coupon Settings',
                'menu_title' => 'Coupon',
                'parent_slug' => 'theme-general-settings',
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'Topics Settings',
                'menu_title' => 'Topics',
                'parent_slug' => 'theme-general-settings',
            ));


            acf_add_options_sub_page(array(
                'page_title' => 'Ieri Settings',
                'menu_title' => 'Ieri',
                'parent_slug' => 'theme-general-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' => 'Typi Mgazine',
                'menu_title' => 'Typi Mgazine',
                'parent_slug' => 'theme-general-settings',
                'menu_slug' => 'acf-options-community-book'
            ));
            acf_add_options_sub_page(array(
                'page_title' => 'Speciali Sidebar',
                'menu_title' => 'Speciali Sidebar',
                'parent_slug' => 'theme-general-settings',
                'menu_slug' => 'acf-options-speciali-sidebar'
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'Agenda Homepage Settings',
                'menu_title' => 'Agenda Homepage',
                'parent_slug' => 'theme-general-settings',
                'menu_slug' => 'acf-options-oggi-e-domani'
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'Ci Ha Lasciato',
                'menu_title' => 'Ci Ha Lasciato',
                'parent_slug' => 'theme-general-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' => 'Sidebar Settings',
                'menu_title' => 'Sidebar',
                'parent_slug' => 'theme-general-settings',
            ));
            acf_add_options_sub_page(array(
                'page_title' => 'Call to action Contributi Utenti',
                'menu_title' => 'Call to action Contributi Utenti',
                'parent_slug' => 'theme-general-settings',
            ));

        }


        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page(array(
                'page_title' => 'Configurazioni generali',
                'menu_title' => 'Configurazioni',
                'menu_slug' => 'theme-configurazioni-generali',
                'capability' => 'edit_posts',
                'redirect' => true
            ));

            acf_add_options_sub_page(array(
                'page_title' => 'General Settings',
                'menu_title' => 'General',
                'parent_slug' => 'theme-configurazioni-generali',
            ));



        }


    }



if (get_bloginfo('url')!='http://romah24.dev/typinews/'):
    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array (
            'key' => 'group_58b6ed5b8e0a1',
            'title' => 'Vetrina',
            'fields' => array (
                array (
                    'sub_fields' => array (
                        array (
                            'default_value' => '',
                            'maxlength' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'key' => 'field_58b6ed85e047b',
                            'label' => 'Titolo',
                            'name' => 'titolo',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                        ),
                        array (
                            'default_value' => '',
                            'new_lines' => '',
                            'maxlength' => 100,
                            'placeholder' => '',
                            'rows' => '',
                            'key' => 'field_58b6ed9fe047c',
                            'label' => 'Descrizione',
                            'name' => 'descrizione',
                            'type' => 'textarea',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                        ),
                        array (
                            'return_format' => 'array',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                            'key' => 'field_58b6edc0e047d',
                            'label' => 'Immagine',
                            'name' => 'immagine',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                        ),
                        array (
                            'default_value' => '',
                            'maxlength' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'key' => 'field_58b6edcce047e',
                            'label' => 'Prezzo',
                            'name' => 'prezzo',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                        ),
                        array (
                            'default_value' => '',
                            'placeholder' => '',
                            'key' => 'field_58b6ede1e047f',
                            'label' => 'Link Acquisto',
                            'name' => 'link_acquisto',
                            'type' => 'url',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                        ),
                    ),
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'table',
                    'button_label' => '',
                    'collapsed' => '',
                    'key' => 'field_58b6ed60e047a',
                    'label' => 'Vetrina - Oggetti',
                    'name' => 'borsino_oggetti',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'acf-options-vetrina',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

    endif;

    if( function_exists('acf_add_local_field_group') ):

//        acf_add_local_field_group(array (
//            'key' => 'group_58eca6810a524',
//            'title' => 'Campi news typi',
//            'fields' => array (
//                array (
//                    'post_type' => array (
//                        0 => 'itinerari',
//                    ),
//                    'taxonomy' => array (
//                    ),
//                    'min' => '',
//                    'max' => '',
//                    'filters' => array (
//                        0 => 'search',
//                        1 => 'post_type',
//                        2 => 'taxonomy',
//                    ),
//                    'elements' => '',
//                    'return_format' => 'object',
//                    'key' => 'field_58eca68673f51',
//                    'label' => 'Itinerari Correlati',
//                    'name' => 'itinerari_correlati',
//                    'type' => 'relationship',
//                    'instructions' => '',
//                    'required' => 0,
//                    'conditional_logic' => 0,
//                    'wrapper' => array (
//                        'width' => '',
//                        'class' => '',
//                        'id' => '',
//                    ),
//                ),
//            ),
//            'location' => array (
//                array (
//                    array (
//                        'param' => 'post_type',
//                        'operator' => '==',
//                        'value' => 'news',
//                    ),
//                ),
//            ),
//            'menu_order' => 0,
//            'position' => 'normal',
//            'style' => 'default',
//            'label_placement' => 'top',
//            'instruction_placement' => 'label',
//            'hide_on_screen' => '',
//            'active' => 1,
//            'description' => '',
//        ));

    endif;
    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array (
            'key' => 'group_58ecae5c78656',
            'title' => 'Speciali Sidebar',
            'fields' => array (
                array (
                    'sub_fields' => array (
                        array (
                            'post_type' => array (
                                0 => 'speciali',
                            ),
                            'taxonomy' => array (
                            ),
                            'allow_null' => 0,
                            'multiple' => 0,
                            'return_format' => 'object',
                            'ui' => 1,
                            'key' => 'field_58ecae73846d8',
                            'label' => 'Speciale',
                            'name' => 'speciale',
                            'type' => 'post_object',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                        ),
                    ),
                    'min' => 0,
                    'max' => 4,
                    'layout' => 'table',
                    'button_label' => '',
                    'collapsed' => '',
                    'key' => 'field_58ecae63846d7',
                    'label' => 'Speciali Sidebar',
                    'name' => 'speciali_sidebar',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'acf-options-speciali-sidebar',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

    endif;
endif;
