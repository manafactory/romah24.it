<?php

add_action( 'init', 'register_custom_post_type_child_function');
function register_custom_post_type_child_function()
{


    register_post_type('itinerari', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Itinerari', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Itinerario', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti gli itinerari', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo itinerario', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica itinerario', /*  Testo per modifica */
            'new_item' => 'Nuovo itinerario', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza itinerari', /* Testo per visualizzare */
            'search_items' => 'Cerca itinerari', /* Testo per la ricerca*/
            'not_found' => 'Nessun itinerario trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessun itinerario trovata nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Itinerari', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-palmtree', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => '', /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */


    $labels = array(
        'name' => __('Categorie Itinerari'),
        'singular_name' => __('Categoria itinerari'),
        'search_items' => __('Ricerca Categoria'),
        'popular_items' => __('Categoria popolari'),
        'all_items' => __('Tutte le Categoria'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Modifica Categoria'),
        'update_item' => __('Aggiorna Categoria'),
        'add_new_item' => __('Aggiungi nuova Categoria'),
        'new_item_name' => __('Nuovo Nome Categoria'),
        'separate_items_with_commas' => __('Separa le Categoria da Categoria'),
        'add_or_remove_items' => __('Aggiungi o rimuovi Categoria'),
        'choose_from_most_used' => __('Scegli tra le Categoria più usate'),
        'not_found' => __('Nessuna Categoria trovata.'),
        'menu_name' => __('Categorie'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'categoria_itinerari'),
    );

    register_taxonomy('categoria_itinerari', array('itinerari'), $args);

}




add_action('init', 'unregister_my_posttype', 20);

function unregister_my_posttype(){
    unregister_post_type( 'community-book' );
    unregister_post_type( 'itinerari' );
}
