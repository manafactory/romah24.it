<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 01/03/17
 * Time: 15.13
 */

require_once('widget/widget_register_child.php');
require_once('inc/register_custom_post_type_child.php');
require_once('inc/acf_child.php');


add_image_size('borsino', 100, 200, true);



    function eventi_oggi_e_domani(){
        $eventi_oggi_e_domani=get_field('eventi_oggi_e_domani','option');

        $eventi_oggi_e_domani_counter=count($eventi_oggi_e_domani);


        if ($eventi_oggi_e_domani_counter>0 && $eventi_oggi_e_domani):
            foreach ($eventi_oggi_e_domani as $evento):
                $eventi_oggi_e_domani_array_post[]=$evento['evento_oggi_e_domani'];
            endforeach;
        endif;
        $eventi_oggi_e_domani=$eventi_oggi_e_domani_array_post;



        return $eventi_oggi_e_domani;
    }



    function eventi_futuri(){



        $eventi_futuri=get_field('eventi_futuri','option');
        $eventi_futuri_counter=count($eventi_futuri);

        if ($eventi_futuri_counter>0):
            if ($eventi_futuri):
                foreach ($eventi_futuri as $evento):
                    $eventi_futuri_array_post[]=$evento['evento_futuro'];
                endforeach;
            endif;
        endif;

        $eventi_futuri=$eventi_futuri_array_post;



        return $eventi_futuri;
    }



